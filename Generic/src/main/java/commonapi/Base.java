package commonapi;

import com.google.common.collect.ImmutableMap;
import org.openqa.selenium.JavascriptExecutor;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.net.URL;

/**
 * Created by mdalam on 12/3/16.
 */
public class Base {

    public WebDriver driver = null;
    public Logger log = Logger.getLogger(Base.class.getName());
    public static final int oneSec = 1000;
    public static final int shortWait = 3000;
    public static final int longWait = 10000;
    public static String app = "";
    public static String env = "";
    public final boolean active = true;
    public final boolean inactive = false;
    private WebDriverWait waitSeconds = null;

    @Parameters({"useSauceLab", "userName", "key", "appUrl", "os", "browserName", "browserVersion"})
    @BeforeMethod
    public void setUp(@Optional("TestNG Examples") boolean useSauceLab, @Optional("TestNG Examples") String userName, @Optional("TestNG Examples") String key, @Optional("TestNG Examples") String appUrl, @Optional("TestNG Examples") String os, @Optional("TestNG Examples") String browserName, @Optional("TestNG Examples") String browserVersion) throws IOException, InterruptedException {
        setApp(appUrl);
        setEnv(appUrl);

        if (useSauceLab == true) {
            getSauceLabDriver(userName, key, os, browserName, browserVersion);
        } else {
            getLocalDriver(os, browserName);
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.navigate().to(appUrl);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.navigate().refresh();  //added by Vrunda
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        System.out.println("-------------------------------- TRYING TO PLACE THE ORDER IN " + app + " ------------------------------------");
        log.info("browser loading the App");
    }

    @AfterMethod
    public void cleanUp(ITestResult result) throws InterruptedException {
        if (ITestResult.FAILURE == result.getStatus()) {
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            try {
                TakesScreenshot ts = (TakesScreenshot) driver;
                File source = ts.getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(source, new File("Applications/test_output/screenshot/" + app + "_" + timeStamp + ".png"));
                System.out.println("Screenshot taken");
            } catch (Exception e) {
                System.out.println("Exception while taking screenshot " + e.getMessage());
            }
        }
        driver.manage().deleteAllCookies();
        log.info("driver is quiting");
        driver.quit();
    }

    public WebDriver getSauceLabDriver(String userName, String key, String os, String browserName,
                                       String browserVersion) throws IOException {

        String URL = "https://" + userName + ":" + key + "@ondemand.saucelabs.com:443/wd/hub";
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platform", os);
        caps.setBrowserName(browserName);
        caps.setCapability("version", browserVersion);
        driver = new RemoteWebDriver(new URL(URL), caps);
        return driver;
    }

    public WebDriver getLocalDriver(String os, String browserName) throws IOException {
        if (os.equalsIgnoreCase("mac") && browserName.equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        }

        if (os.equalsIgnoreCase("mac") && browserName.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "/Users/malam2/Desktop/FootLocker/Generic/selenium-browser-driver/chromedriver");


//            ChromeOptions options = new ChromeOptions();
//            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//
//            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);

            driver = new ChromeDriver();

//
//            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//
//            capabilities.setCapability("chrome.binary", "/Users/malam2/Desktop/FootLocker/Generic/selenium-browser-driver/chromedriver");
//
//            driver = new ChromeDriver(capabilities);


//            ChromeDriverService service = new ChromeDriverService.Builder()
//                    .usingDriverExecutable(new File("/Users/malam2/Desktop/FootLocker/Generic/selenium-browser-driver/chromedriver"))
//                    .usingAnyFreePort()
//                    .withEnvironment(ImmutableMap.of("DISPLAY",":10"))
//                    .build();
//            service.start();
//
////then start driver, with URL mapped to above-started service URL
//
//            DesiredCapabilities dc = DesiredCapabilities.chrome();
//            ChromeOptions options = new ChromeOptions();
//            options.addArguments("--start-maximized");
//            dc.setCapability(ChromeOptions.CAPABILITY,options);
//            driver = new RemoteWebDriver(service.getUrl(), dc);







//            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//            RemoteWebDriver driver = new RemoteWebDriver(
//                    new URL("http://localhost:4444/wd/hub"), capabilities);
        }

        if (os.equalsIgnoreCase("mac") && browserName.equalsIgnoreCase("safari")) {
            DesiredCapabilities desiredCapabilities = DesiredCapabilities.safari();
            SafariOptions safariOptions = new SafariOptions();
            safariOptions.setUseCleanSession(true);
            desiredCapabilities.setCapability(SafariOptions.CAPABILITY, safariOptions);
            driver = new SafariDriver();
        }
        return driver;
    }

    public String getPropertyValue(String key) {
        Properties properties = new Properties();
        InputStream input = null;
        String value = "";
        try {
            input = Base.class.getClassLoader().getResourceAsStream("config.properties");
            properties.load(input);
            value = properties.getProperty(key);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return value;
        }
    }

    public void getPropertyValue2() {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            String filename = "config.properties";
            input = Base.class.getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                return;
            }
            prop.load(input);
            //get the property value and print it out
            System.out.println(prop.getProperty("database"));
            System.out.println(prop.getProperty("dbuser"));
            System.out.println(prop.getProperty("dbpassword"));
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void typeByCssThenEnter(String locator, String value) {
        driver.findElement(By.cssSelector(locator)).sendKeys(value, Keys.ENTER);
    }

    public void switchWindow() {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
            driver.manage().window().maximize();
        }
    }

    public void clickUsingJavascriptExecutor(WebElement element) {
        try {
            JavascriptExecutor javascript = (JavascriptExecutor) driver;
            javascript.executeScript("arguments[0].click();", element);
        } catch (Exception objException) {
            objException.printStackTrace();
        }
    }

    public void switchIframe(String strIframe) {
        try {
            driver.switchTo().frame(strIframe);
        } catch (Exception objException) {
            objException.printStackTrace();
        }
    }

    public static int getRowCount(String sheetName) throws IOException {
        String filePath = "/Users/c001217/Documents/FOOTLOCKER/FL_Automation_Framework/Applications/src/main/resources/TestData_PROD.xlsx";
        FileInputStream fis = new FileInputStream(filePath);
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(fis);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sh = wb.getSheet(sheetName);
        int rowCount = sh.getLastRowNum() + 1;
        return rowCount;
    }

    public static int getcellCount(String sheetName, int rowNum) throws InvalidFormatException, IOException {
        String filePath = "/Users/malam2/Desktop/FootLocker/Applications/src/main/resources/TestData_PROD.xlsx";
        FileInputStream fis = new FileInputStream(filePath);
        Workbook wb = WorkbookFactory.create(fis);
        Sheet sh = wb.getSheet(sheetName);
        Row row = sh.getRow(rowNum);
        return row.getLastCellNum();
    }

    public static String getExcelData(String sheetName, int rowNum, int colNum) throws InvalidFormatException, IOException {
        String filePath = "/Users/malam2/Desktop/FootLocker/Applications/src/main/resources/TestData_PROD.xlsx";
        FileInputStream fis = new FileInputStream(filePath);
        Workbook wb = WorkbookFactory.create(fis);
        Sheet sh = wb.getSheet(sheetName);
        Row row = sh.getRow(rowNum);
        String data = row.getCell(colNum).getStringCellValue();
        return data;
    }

    public static String getExcelData(String sheetName, String Brand, String FullfillmentType) throws IOException {
        String data = null;
        String filePath = "/Users/malam2/Desktop/FootLocker/Applications/src/main/resources/TestData_PROD.xlsx";
        FileInputStream fis = new FileInputStream(filePath);
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(fis);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sh = wb.getSheet(sheetName);
        int rowcount = 0;
        rowcount = getRowCount(sheetName);

        for (int r = 1; r < rowcount; r++) {
            Row row = sh.getRow(r);
            if (row.getCell(0).getStringCellValue().toLowerCase().equals(Brand.toLowerCase())) {
                int col = row.getLastCellNum();
                for (int c = 1; c < col; c++) {
                    if (row.getCell(c).getStringCellValue().toLowerCase().equals(FullfillmentType.toLowerCase())) {
                        row = sh.getRow(r + 1);
                        data = row.getCell(c).getStringCellValue();
                        break;
                    }
                }
            }
        }
        return data;
    }


    // modified and added by Vrunda
    public static String getXLSXTestData(String FileName, String SheetName, String RowId, String column) throws IOException {
        // String filePath = "Applications/src/main/resources/TestData.xlsx";
        String filePath = "/Users/malam2/Desktop/FootLocker/Applications/src/main/resources/TestData_PROD.xlsx";

        String col1 = null;
        DataFormatter df = new DataFormatter();
        FileInputStream file = new FileInputStream(new File(FileName));
        System.out.println("the filepath is  " + file);
        System.out.println("the sheetname is  " + SheetName);
        System.out.println("the RowId is  " + RowId);
        System.out.println("the column is  " + column);
        XSSFWorkbook book = new XSSFWorkbook(file);
        XSSFSheet sheet = book.getSheet(SheetName);
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        for (int rowIterator = 1; rowIterator <= rowCount; rowIterator++) {
            String row = sheet.getRow(rowIterator).getCell(0).getStringCellValue();
            if (row.equalsIgnoreCase(RowId)) {
                for (int colIterator = 1; colIterator < sheet.getRow(rowIterator).getLastCellNum(); colIterator++) {
                    String col = sheet.getRow(0).getCell(colIterator).getStringCellValue();
                    if (col.equalsIgnoreCase(column)) {
                        Cell cellvalue = sheet.getRow(rowIterator).getCell(colIterator);
                        col1 = df.formatCellValue(cellvalue);
                        break;
                    }
                }
            }
        }
        return col1;
    }

    public static String readFromExcel(String fileRef, String sheetRef, String cellRef) throws IOException {
        FileInputStream fis = new FileInputStream(fileRef);
        Workbook wb = new XSSFWorkbook(fis);
        Sheet sheet = wb.getSheet(sheetRef);
        DataFormatter formatter = new DataFormatter();
        CellReference cellReference = new CellReference(cellRef);
        Row row = sheet.getRow(cellReference.getRow());
        Cell cell = row.getCell(cellReference.getCol());
        String value = "";
        if (cell != null) {
            value = formatter.formatCellValue(cell);//cell.getStringCellValue();
        }
        return value;
    }

    public void clickAfterElementPresent(WebElement element) throws InterruptedException {
        waitSeconds = new WebDriverWait(driver, 30);
        waitSeconds.until(ExpectedConditions.visibilityOf(element)).click();
        Thread.sleep(500);
    }

    public void clickAfterElementPresent_UAT(WebElement element) throws InterruptedException {
        waitSeconds = new WebDriverWait(driver, 50);
        waitSeconds.until(ExpectedConditions.visibilityOf(element)).click();
        Thread.sleep(1000);
    }

    public void submitAfterElementPresent(WebElement element) throws InterruptedIOException {
        waitSeconds = new WebDriverWait(driver, 50);
        waitSeconds.until(ExpectedConditions.visibilityOf(element)).submit();

    }

    public void typeAfterElementPresent(WebElement element, String str) throws InterruptedException {
        waitSeconds = new WebDriverWait(driver, 30);
        //String del = Keys.chord(Keys.CONTROL+ "a") + Keys.DELETE;
        waitSeconds.until(ExpectedConditions.visibilityOf(element)).clear();
        waitSeconds.until(ExpectedConditions.visibilityOf(element)).sendKeys(str);
    }

    public String getTextFromLabelAfterElementPresent(WebElement element) throws InterruptedException {
        waitSeconds = new WebDriverWait(driver, 30);
        //Thread.sleep(1000);
        return waitSeconds.until(ExpectedConditions.visibilityOf(element)).getText();
    }

    public String getTextFromButtonAfterElementPresent(WebElement element) throws InterruptedException {
        waitSeconds = new WebDriverWait(driver, 30);
        Thread.sleep(oneSec);
        return waitSeconds.until(ExpectedConditions.visibilityOf(element)).getAttribute("value");
    }

    public void selectFromDropdown(WebElement element, int index) {
        Select dropdown = new Select(element);
        dropdown.selectByIndex(index);
    }

    public void scrollDown() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 250);");
    }

    public void setApp(String initApp) {
        if (initApp.equals("http://www.eastbay.com/") || initApp.equals("http://eastbay.uat.msp.guidance.com/"))
            app = "EASTBAY";
        else if (initApp.equals("http://www.footlocker.com/") || initApp.equals("http://footlocker.uat.msp.guidance.com/"))
            app = "FOOTLOCKER";
        else if (initApp.equals("http://www.ladyfootlocker.com/") || initApp.equals("http://ladyfootlocker.uat.msp.guidance.com/"))
            app = "LADYFOOTLOCKER";
        else if (initApp.equals("http://www.kidsfootlocker.com/") || initApp.equals("http://kidsfootlocker.uat.msp.guidance.com/"))
            app = "KIDSFOOTLOCKER";
        else if (initApp.equals("http://www.footaction.com/") || initApp.equals("http://footaction.uat.msp.guidance.com/"))
            app = "FOOTACTION";
        else if (initApp.equals("http://www.six02.com/") || initApp.equals("http://six02.uat.msp.guidance.com/"))
            app = "SIX02";
        else if (initApp.equals("http://www.champssports.com/") || initApp.equals("http://champssports.uat.msp.guidance.com/"))
            app = "CHAMPSSPORTS";
        else if (initApp.equals("http://www.final-score.com/") || initApp.equals("http://finalscore.uat.msp.guidance.com/"))
            app = "FINAL-SCORE";
        else if (initApp.equals("http://www.footlocker.ca/") || initApp.equals("http://footlocker-ca.uat.msp.guidance.com/"))
            app = "FOOTLOCKERCA";
        else if (initApp.equals("http://www.eastbayteamsales.com/") || initApp.equals("http://teamsales.uat.msp.guidance.com/"))
            app = "EASTBAYTEAMSALES";
        else if (initApp.equals("http://m.eastbay.com/") || initApp.equals("http://m-eastbay.uat.msp.guidance.com/"))
            app = "EASTBAY_M";
        else if (initApp.equals("http://m.footlocker.com/") || initApp.equals("http://m-footlocker.uat.msp.guidance.com/"))
            app = "FOOTLOCKER_M";
        else if (initApp.equals("http://m.ladyfootlocker.com/") || initApp.equals("http://m-ladyfootlocker.uat.msp.guidance.com/"))
            app = "LADYFOOTLOCKER_M";
        else if (initApp.equals("http://m.kidsfootlocker.com/") || initApp.equals("http://m-kidsfootlocker.uat.msp.guidance.com/"))
            app = "KIDSFOOTLOCKER_M";
        else if (initApp.equals("http://m.footaction.com/") || initApp.equals("http://m-footaction.uat.msp.guidance.com/"))
            app = "FOOTACTION_M";
        else if (initApp.equals("http://m.six02.com/") || initApp.equals("http://m-six02.uat.msp.guidance.com/"))
            app = "SIX02_M";
        else if (initApp.equals("http://m.champssports.com/") || initApp.equals("http://m-champssports.uat.msp.guidance.com/"))
            app = "CHAMPSSPORTS_M";
        else if (initApp.equals("http://m.final-score.com/") || initApp.equals("http://m-finalscore.uat.msp.guidance.com/"))
            app = "FINAL-SCORE_M";
        else if (initApp.equals("http://m.footlocker.ca/") || initApp.equals("http://m-footlocker-ca.uat.msp.guidance.com/"))
            app = "FOOTLOCKERCA_M";
        else app = "Invalid URL or Non-Footlocker App";
    }

    public void setEnv(String initEnv) {
        if (initEnv.contains("uat")) env = "UAT";
        else env = "PROD";
    }

    public static ResultSet getResult(String query) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // DBUtil.close(ps);
            // DBUtil.close(connection);
        }
        return rs;
    }
}