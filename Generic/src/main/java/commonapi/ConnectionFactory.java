package commonapi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	private static ConnectionFactory instance = new ConnectionFactory();
	public static final String URL = "jdbc:oracle:thin:@//dldb-azrxms01.walgreens.com:1521/drxrenew";
	
	public static final String DRIVER_CLASS = "oracle.jdbc.driver.OracleDriver"; 
	public static final String USER = "QE1_CORE_SERV";
	public static final String PASSWORD = " SERV1090N34MK1LKRMWAPQE";
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			System.out.println("ERROR: Unable to Connect to Database.");
		}
		return connection;
	}	
	
	public static Connection getConnection() {
		return instance.createConnection();
	}
}
