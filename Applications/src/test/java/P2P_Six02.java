import org.testng.annotations.Test;
import testapi.Six02;

/**
 * Created by mdalam on 12/3/16.
 */
public class P2P_Six02 extends Six02 {

    @Test(priority = 1, enabled = inactive)
    public void testGeneral() throws Exception {
        runGeneral();
    }

    @Test(priority = 2, enabled = inactive)
    public void testBOSS() throws Exception {
        runBOSS();
    }

    @Test(priority = 3, enabled = inactive)
    public void testDCOrder() throws Exception {
        runDCOrder();
    }

    @Test(priority = 4, enabled = active)
    public void testDropship() throws Exception {
        runDropship();
    }
}