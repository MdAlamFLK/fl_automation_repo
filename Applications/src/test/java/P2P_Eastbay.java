import testapi.Eastbay;

import org.testng.annotations.Test;

/**
 * Created by mdalam on 12/3/16.
 */
public class P2P_Eastbay extends Eastbay {

    @Test(priority = 1, enabled = inactive)
    public void testGeneral() throws Exception {
        runGeneral();
    }

    @Test(priority = 2, enabled = inactive)
    public void testBOSS() throws Exception {
        runBOSS();
    }

    @Test(priority = 3, enabled = active)
    public void testDCOrder() throws Exception {
        runDCOrder();
    }

    @Test(priority = 4, enabled = inactive)
    public void testDropship() throws Exception {
        runDropship();
    }
}