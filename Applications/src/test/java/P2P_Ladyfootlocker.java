import org.testng.annotations.Test;
import testapi.Ladyfootlocker;

/**
 * Created by mdalam on 12/3/16.
 */
public class P2P_Ladyfootlocker extends Ladyfootlocker {

    @Test(priority = 1, enabled = inactive)
    public void testGeneral() throws Exception {
        runGeneral();
    }

    @Test(priority = 2, enabled = inactive)
    public void testBOSS() throws Exception {
        runBOSS();
    }

    @Test(priority = 3, enabled = inactive)
    public void testS2S() throws Exception {
        runS2S();
    }

    @Test(priority = 4, enabled = inactive)
    public void testBORIS() throws Exception {
        runBORIS();
    }

    @Test(priority = 5, enabled = inactive)
    public void testDCOrder() throws Exception {
        runDCOrder();
    }


    @Test(priority = 6, enabled = inactive)
    public void testSSI() throws Exception {
        runSSI();
    }

    @Test(priority = 7, enabled = active)
    public void testDropship() throws Exception {
        runDropship();
    }
}