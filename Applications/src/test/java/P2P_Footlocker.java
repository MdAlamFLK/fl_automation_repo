import org.testng.annotations.Test;
import testapi.Footlocker;

/**
 * Created by mdalam on 12/3/16.
 */
public class P2P_Footlocker extends Footlocker {

    @Test(priority = 1, enabled = inactive)
    public void testGeneral() throws Exception {
        addToCart();
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    @Test(priority = 2, enabled = inactive)
    public void testBOSS() throws Exception {
        runBOSS();
    }

    @Test(priority = 3, enabled = inactive)
    public void testS2S() throws Exception {
        runS2S();
    }

    @Test(priority = 4, enabled = inactive)
    public void testBORIS() throws Exception {
        runBORIS();
    }

    @Test(priority = 5, enabled = inactive)
    public void testDCOrder() throws Exception {
        runDCOrder();
    }

    @Test(priority = 6, enabled = inactive)
    public void testSDD() throws Exception {
        runSDD();
    }

    @Test(priority = 7, enabled = inactive)
    public void testSSI() throws Exception {
        runSSI();
    }

    @Test(priority = 8, enabled = active)
    public void testDropship() throws Exception {
        runDropship();
    }
}