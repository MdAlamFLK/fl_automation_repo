import org.testng.annotations.Test;
import testapi.FinalScore;

/**
 * Created by mdalam on 12/3/16.
 */
public class P2P_FinalScore extends FinalScore {

    @Test(priority = 1, enabled = inactive)
    public void testGeneral() throws Exception {
        runGeneral();
    }

    @Test(priority = 2, enabled = inactive)
    public void testDCOrder() throws Exception {
        runDCOrder();
    }
}