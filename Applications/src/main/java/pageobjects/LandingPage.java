package pageobjects;

import testapi.SubBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by malam2 on 12/10/16.
 */
public class LandingPage extends SubBase {

    private static WebElement element = null;

    public static WebElement search_link(WebDriver driver){
        if (app == "FOOTLOCKER") element = driver.findElement(By.id("newSearchBarComplete"));
        else if (app == "EASTBAY") element = driver.findElement(By.id("header_search_button"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "SIX02")element = driver.findElement(By.xpath(".//*[@id='search_drop']"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='open_search']/p"));
        return element;
    }

    public static WebElement search_Textbox(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.id("reduce_input_text_height"));
        else if (app == "EASTBAY") element = driver.findElement(By.id("search"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='searchKeywordInput']"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        return element;
    }

    public static WebElement submit_link(WebDriver driver){
        if (app == "FOOTLOCKER") element = driver.findElement(By.cssSelector("#search_field_submit"));
        else if (app == "EASTBAY") element = driver.findElement(By.cssSelector("#search_submit_button"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.cssSelector("#submitButtton"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='kfl-advancedsearch-wrap']/a"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='js-search']/form/div"));
        else if (app == "SIX02")element = driver.findElement(By.xpath(".//*[@id='header_search_button']"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='sbmt-btn']"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='form_move']/div[2]/input"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='header_search_button']/img"));
        return element;
    }


    ////////////////////////////////////////////////////////////////////////////////
    //////////////////// CODE REVISION COMPLETED ABOVE THIS LINE ///////////////////
    ////////////////////////////////////////////////////////////////////////////////



















    public static WebElement shop_link(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='header_nav_shop']"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='navigation-links']/ul[1]/li[2]/a"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='lfl_logo']/a/img"));
       // else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='kfl-wrapper']/div[4]/ul/li[1]/a/span"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='kfl-wrapper']/div[3]/ul/li[1]/a"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='shop']/a"));
        else if (app == "SIX02") element = driver.findElement(By.xpath("/html/body/div[5]/header/nav/ul/li[2]/a"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='header_nav']/div[1]"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='menu-tabs']/ul/li[1]/a/span"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='global-nav']/ul/li[1]/a"));
        else if (app == "EASTBAY_M") element = driver.findElement(By.cssSelector("#header_navigation_link"));
        else if (app == "FOOTLOCKER_M") element = driver.findElement(By.cssSelector("#flyoutLink"));
        else if (app == "LADYFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='flyoutLink']/span"));
        else if (app == "KIDSFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='header_menu_toggle']/i"));
        else if (app == "FOOTACTION_M") element = driver.findElement(By.xpath(".//*[@id='flyoutLink']"));
        return element;
    }

    public static WebElement category_link(WebDriver driver){
        if (app == "EASTBAY") element = driver.findElement(By.cssSelector("#main_nav_men_link"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='navigation-dropdown']/div[1]/div[1]/ul/li[2]/a"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='lfl_logo']/a/img"));
//        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='kfl-wrapper']/div[4]/div[1]/ul[1]/li[1]/a/span"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='kfl-wrapper']/div[3]/div[1]/ul[1]/li[1]/a"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='shop_ex_main']/li[1]/a"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='navigation_dropdown']/div[1]/div[3]/ul/li[1]/a/span[2]"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='nav_categories']/div/li[1]"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='menu-tabs']/ul/li[1]/a/span"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='global-nav']/ul/li[1]/a"));
        else if (app == "EASTBAY_M") element = driver.findElement(By.xpath(".//*[@id='shop_nav_accordion']/li[1]/div"));
        else if (app == "FOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul[1]/li[4]/a"));
        else if (app == "LADYFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul/li[2]/a"));
        else if (app == "KIDSFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='mobile_menu']/div/ul[1]/li[2]/a"));
        else if (app == "FOOTACTION_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul[2]/li[1]/a"));
        return element;
    }

    public static WebElement option_link(WebDriver driver){
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='main_nav_menu_men']/div/div[1]/ul[1]/li[1]/a"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='navigation-dropdown']/div[1]/div[2]/div[2]/div[1]/ul[1]/li[1]/a"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='sticker_menu_container']/div[1]/ul/li/div[1]/a[1]"));
//        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='boys-shoes']/ul[1]/li[3]/a"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath("//*[@id='boys-shoes']/ul[1]/li[2]/a"));
//        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='shop_nav_list']/div/div/div[1]/div[1]/ul/li[3]/a"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='shop_nav_list']/div/div/div[1]/div[1]/ul/li[4]/a"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='navigation_dropdown']/div[1]/div[3]/ul/li[1]/ul/li[1]/a"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='nav_mens']/ul/li[2]/a/img"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='menu-tabs']/ul/li[1]/a/span"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='group_Product_Type']/li[1]/a"));
        else if (app == "EASTBAY_M") element = driver.findElement(By.xpath(".//*[@id='shop_nav_accordion']/li[1]/ul/li[1]/ul/li[2]/a"));
        else if (app == "FOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul[1]/li[4]/ul/li[1]/ul/li[1]/a"));
        else if (app == "LADYFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul/li[2]/ul/li[1]/a"));
        else if (app == "KIDSFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='mobile_menu']/div/ul[1]/li[2]/ul/li[1]/ul/li[1]/a"));
        else if (app == "FOOTACTION_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul[2]/li[1]/ul/li[8]/a"));
        return element;
    }

    public static WebElement section_link(WebDriver driver) {
        if (app == "EASTBAY_M") element = driver.findElement(By.xpath(".//*[@id='shop_nav_accordion']/li[1]/ul/li[1]/div"));
        else if (app == "FOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul[1]/li[4]/ul/li[1]/a"));
        else if (app == "LADYFOOTLOCKER_M") element = driver.findElement(By.cssSelector(""));
        //else if (app == "KIDSFOOTLOCKER_M") element = driver.findElement(By.xpath(".//*[@id='mobile_menu']/div/ul[1]/li[2]/ul/li[1]/a"));
        else if (app == "FOOTACTION_M") element = driver.findElement(By.xpath(".//*[@id='flyout-navigation']/div/ul[2]/li[1]/a"));
        return element;
    }

    public static WebElement search_link_UAT(WebDriver driver){
        if (app == "FOOTLOCKER") element = driver.findElement(By.id("newSearchBarComplete"));
        else if (app == "EASTBAY") element = driver.findElement(By.id("header_search_button"));
        else if (app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "FOOTLOCKERCA")
         element = driver.findElement(By.xpath(".//*[@id='reduce_input_text_height']"));
        else if (app == "SIX02")element = driver.findElement(By.id("search_drop"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='open_search']/p"));

        return element;
    }






}