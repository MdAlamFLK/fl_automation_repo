package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testapi.SubBase;

/**
 * Created by malam2 on 12/10/16.
 */
public class HomePage extends SubBase {

    private static WebElement element = null;

    public static WebElement user_icon(WebDriver driver){
        element = driver.findElement(By.xpath(".//*[@id='header_account_link']/span"));
        return element;
    }

    public static WebElement userId_field(WebDriver driver){
        element = driver.findElement(By.cssSelector("#login_email"));
        return element;
    }

    public static WebElement password_field(WebDriver driver){
        element = driver.findElement(By.cssSelector("#login_password"));
        return element;
    }

    public static WebElement login_button(WebDriver driver){
        element = driver.findElement(By.cssSelector("#login_submit"));
        return element;
    }

    public static WebElement viewCartAndCheckout_button(WebDriver driver) {
        By initElement;
        if (app.equals("FOOTLOCKER")) {
            initElement = By.xpath(".//*[@id='miniAddToCart_actions']/div[1]/a[2]");
        } else if (app.equals("KIDSFOOTLOCKER")) {
            initElement = By.xpath(".//*[@id='miniAddToCart_actions']/div[1]/a[2]");

        } else if (app.equals("EASTBAY")) {
            initElement = By.cssSelector("#push_mc_checkout_button");

        } else if (app.equals("FOOTACTION")) {
            initElement = By.xpath(".//*[@id='miniAddToCart_actions']/div[1]/a[2]");

        } else if (app.equals("SIX02")) {
            initElement = By.xpath(".//*[@id='miniAddToCart_actions']/div[1]/a[2]");

        } else if (app.equals("LADYFOOTLOCKER")) {
            initElement = By.xpath(".//*[@id='miniAddToCart_actions']/div[1]/a[2]");

        } else if (app.equals("CHAMPSSPORTS")) {
            //initElement = By.xpath(".//*[@id='flyin_container']/div/div[1]/a");
            initElement = By.xpath("//a[contains(@data-btnname,'cart_checkout')]");

        } else if (app.equals("FINAL-SCORE")) {
            initElement = By.xpath(".//*[@id='miniAddToCart_actions']/div[1]/a[2]");

        } else if (app.equals("EASTBAY_M")) {
            initElement = By.cssSelector("#minicart_fullcart_button");

        } else if (app.equals("FOOTLOCKER_M") || app.equals("LADYFOOTLOCKER_M") || app.equals("KIDSFOOTLOCKER_M")) {
            initElement = By.cssSelector("#fullcart");

        } else if (app.equals("FOOTACTION_M")) {
            initElement = By.xpath(".//*[@id='fullcart']");

        } else {
            initElement = By.cssSelector(".button.cta_button");
        }
        return driver.findElement(initElement);
    }

    public static WebElement cartCheckout_button(WebDriver driver){
        By initElement;
        if (app.equals("FOOTLOCKER") || app.equals("LADYFOOTLOCKER")) {
            initElement = By.xpath(".//*[@id='cart_checkout_button']");

        } else if (app.equals("CHAMPSSPORTS")) {
            initElement = (By.xpath("#billFirstName"));
        } else if (app.equals("KIDSFOOTLOCKER")) {
            initElement = (By.cssSelector("#cart_checkout_button"));
        } else {
            initElement = By.cssSelector("#cart_checkout_button");
        }
        return driver.findElement(initElement);
    }

    public static WebElement subTotal_label(WebDriver driver) {
        By initElement;
        if (app.equals("EASTBAY")) {
            initElement = By.xpath(".//*[@id='push_mc_order_summary']/span[2]/span[1]");

        } else if (app.equals("CHAMPSSPORTS")) {
            initElement = By.xpath("//*[contains(text(), 'subtotal firepath-matching-node')]");

        } else if (app.equals("EASTBAY_M")) {
            initElement = By.cssSelector("#minicart_subtotal_container");

        } else if (app.equals("FOOTLOCKER_M") || app.equals("LADYFOOTLOCKER_M") || app.equals("KIDSFOOTLOCKER_M") || app.equals("FOOTACTION_M")) {
            initElement = By.cssSelector("#mini_subtotal");

        } else {
            initElement = By.xpath(".//*[@id='miniAddToCart_subtotal']/span[1]");
        }
        return driver.findElement(initElement);
    }

    public static WebElement billingAndShippingAddress_label(WebDriver driver) {
        if(app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='billAddressPane_edit']/div[1]/span"));
        return element;
    }

    public static WebElement paymentMethod_label(WebDriver driver) {
        if(app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='paymentMethodPane_edit']/div[1]/span"));
        return element;
    }

    public static WebElement promoCode_label(WebDriver driver) {
        if(app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='promoCodePane_edit']/div[1]/span"));
        return element;
    }
}