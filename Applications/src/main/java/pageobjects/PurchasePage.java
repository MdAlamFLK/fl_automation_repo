package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testapi.SubBase;

/**
 * Created by malam2 on 12/10/16.
 */
public class PurchasePage extends SubBase {
    private static WebElement element = null;


    public static WebElement billFirstName_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billFirstName"));
    }

    public static WebElement billLastName_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billLastName"));
    }

    public static WebElement billStreet_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billAddress1"));
    }

    public static WebElement billApt_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billAddress2"));
    }

    public static WebElement billZipCode_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billPostalCode"));
    }

    public static WebElement billCity_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billCity"));
    }

    public static WebElement billPhone_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billHomePhone"));
    }

    public static WebElement billEmail_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billEmailAddress"));
    }

    public static WebElement shipTo_checkbox(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billPaneShipToBillingAddressContainer>label"));
    }

    public static WebElement shipFirstName_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipFirstName"));
    }

    public static WebElement shipLastName_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipLastName"));
    }

    public static WebElement shipStreet_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipAddress1"));
    }

    public static WebElement shipApt_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipAddress2"));
    }

    public static WebElement shipZipCode_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipPostalCode"));
    }

    public static WebElement shipPhone_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipHomePhone"));
    }

    public static WebElement shipPaneContinue_button(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipPaneContinue"));
    }

    public static WebElement billPaneContinue_button(WebDriver driver) {
        return driver.findElement(By.cssSelector("#billPaneContinue"));
    }

    public static WebElement shipMethodPaneContinue_button(WebDriver driver) {
        return driver.findElement(By.cssSelector("#shipMethodPaneContinue"));
      //  return driver.findElement(By.xpath("//a[@id='shipMethodPaneContinue']/span"));
    }

    public static WebElement cardNumber_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#CardNumber"));
    }

    public static WebElement cardExpireDateMM_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#CardExpireDateMM"));
    }

    public static WebElement cardExpireDateYY_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#CardExpireDateYY"));
    }

    public static WebElement cardCCV_field(WebDriver driver) {
        return driver.findElement(By.cssSelector("#CardCCV"));
    }

    public static WebElement payMethodPaneContinue_button(WebDriver driver) {
        return driver.findElement(By.cssSelector("#payMethodPaneContinue"));
    }

    public static WebElement orderSubmit_label(WebDriver driver) {
        return driver.findElement(By.cssSelector("#orderSubmit"));
    }

    public static WebElement paymentErrorMessage_label(WebDriver driver) {
        return driver.findElement(By.cssSelector("#payMethodPane_error"));
    }

    public static WebElement SDD_TextVerify(WebDriver driver) {
        if (app == "FOOTLOCKER")
//            element = driver.findElement(By.xpath(".//*[@id='shipMethodContainer']/label[contains(@for,'shipMethodSDD0')]"));
//            element = driver.findElement(By.xpath("//div[@id='shipMethodContainer']/label[contains(@for,'shipMethodSDD0')]"));
//            element = driver.findElement(By.xpath(".//*[@id='shipMethodContainer']/label[5]"));
//            element = driver.findElement(By.cssSelector("#shipMethodSDD0"));
            element = driver.findElement(By.xpath("//div[@id='shipMethodContainer']/input[5]"));
        return element;
    }


    public static WebElement BORIS_StorePickupTodayLabelVerify(WebDriver driver){
        if (app == "FOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='shipMethPane_content']/div[1]/div[1]/h4"));
            element = driver.findElement(By.xpath(".//*[@id='shipMethPane_content']/div[1]/div[1]/h4[contains(text(),'Store Pickup TODAY')]"));
        return element;
    }

    public static WebElement BORIS_PickupTodayRadioButtonVerify(WebDriver driver){
        if (app == "FOOTLOCKER")
            //element = driver.findElement(By.xpath(".//*[@id='storePickupTodayOptionLabel']"));
            element = driver.findElement(By.xpath(".//*[@id='storePickupTodayOptionLabel'][contains(text(),'TODAY')]"));

        return element;
    }

}