package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testapi.SubBase;

import java.util.List;

/**
 * Created by Vrunda on 1/25/17.
 */
public class ISAPage extends SubBase {
    private static WebElement element = null;

    public static List<WebElement> locationList_grid(WebDriver driver) {
        List<WebElement> elements = null;
        if (app == "FOOTLOCKER") elements = driver.findElements(By.xpath("//div[@id='storegrid']/ul/li"));
        else if (app == "LADYFOOTLOCKER")elements = driver.findElements(By.xpath("//div[@id='storegrid']/ul/li"));
        else if (app == "KIDSFOOTLOCKER")elements = driver.findElements(By.xpath("//div[@id='storegrid']/ul/li"));
        else if (app == "CHAMPSSPORTS")elements = driver.findElements(By.xpath(".//*[@id='isa_list_view_container']/div"));
        else if (app == "FOOTACTION")elements = driver.findElements(By.xpath("//div[@id='storegrid']/ul/li"));
        return elements;
    }

    public static WebElement findStores_button(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='favorite_stores_find_stores_button']"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='favorite_stores_find_stores_button']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='favorite_stores_find_stores_button']"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='location_lookup_submit']"));
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='favorite_stores_find_stores_button']"));
        return element;
    }

    public static WebElement S2SpayNow_button(WebDriver driver,String elementid) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        //else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='"+elementid+"']/button"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='"+elementid+"']/div[5]/button"));
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        return element;
    }

    public static WebElement BORISpayNow_button(WebDriver driver,String elementid) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='"+elementid+"']/div[5]/button"));
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));
        return element;
    }


    ////////////////////////////////////////////////////////////////////////////////
    //////////////////// CODE REVISION COMPLETED ABOVE THIS LINE ///////////////////
    ////////////////////////////////////////////////////////////////////////////////


    public static WebElement storeLocation_textbox(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='favorite_stores_location_input']"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='favorite_stores_location_input']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='favorite_stores_location_input']"));
      //  else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='isa_location']"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.cssSelector("#isa_location"));
        else if (app == "FOOTACTION")  element = driver.findElement(By.xpath(".//*[@id='favorite_stores_location_input']"));
        return element;
    }



        public static WebElement SDDLocation_list(WebDriver driver,String elementid) {
        if (app == "FOOTLOCKER")
        element = driver.findElement(By.xpath(".//*[@id='favorite_stores_store_name_link_"+elementid+"']"));
        return element;
    }

    public static WebElement SDDLocationOnMap_popUp(WebDriver driver,String elementid) {
        if (app == "FOOTLOCKER")
        element= driver.findElement(By.xpath(".//*[@id='map']/div[1]/div[2]/div[4]/div/div[1]/div/p[contains(@class,'citystatezip')]/span[contains(@id,'favorite_stores_map_zip_"+elementid+"')]"));
        return element;
    }
    public static WebElement SDDpayNow_button(WebDriver driver,String elementid) {
        if (app == "FOOTLOCKER")
        //    element = driver.findElement(By.xpath("//ul/li/div[2][contains(@class,'delivery storepickup_and_ssi')]/form/button"));
     //   //[starts-with(@id,'active_loc_']/button
     //   element = driver.findElement(By.xpath("//ul/li/div[2]/form/input[contains(@value,'PICKUP_IN_STORE')]/../button"));
        element = driver.findElement(By.xpath(".//*[@id='active_loc_"+elementid+"']/button"));

        return element;
    }

    public static WebElement SDD_TextVerify(WebDriver driver) {
        if (app == "FOOTLOCKER")
            //     element = driver.findElement(By.xpath(".//*[@id='shipMethodContainer']/label[contains(@for,'shipMethodSDD0')]"));
            element = driver.findElement(By.xpath(".//*[@id='shipMethodSDD0']"));
        //  element = driver.findElement(By.xpath(".//*[@id='shipMethodContainer']/label[5]"));

        return element;
    }

}
