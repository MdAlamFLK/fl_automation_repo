package pageobjects;

import testapi.SubBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by malam2 on 12/10/16.
 */
public class ShopPage extends SubBase {

    private static WebElement element = null;


    public static WebElement size_link(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.cssSelector("#pdp_size_select_mask"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_size_select_mask']"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_style_S77344']/img"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.id("pdp_size_select_mask"));
        else if (app == "SIX02") element = driver.findElement(By.cssSelector("#pdp_size_select_mask"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='pdp_size_select']"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath("//div/label[@id='lbl_storepickup']"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//a[contains(@id,'size_5')]"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='product_sizes']"));
        return element;
    }

    public static WebElement size_DCOrder(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[3]"));
        else if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[9]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_3_31509001']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[2]"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[4]"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[1]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_7_21351191']"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='size_4_32819101']"));
        else element = driver.findElement(By.xpath(""));
        return element;
    }

    public static WebElement size_BOSS(WebDriver driver) {
        if (app == "FOOTLOCKER") element =  driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[4]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_12_S77344']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[2]"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[3]"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[3]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_2_42964026']"));
        else if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[8]"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='product_sizes']/option[1]"));
        else element = driver.findElement(By.xpath(""));
        return element;
    }

    public static WebElement size_S2S(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[9]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(" "));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[2]"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[1]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_10_08317003']"));
        return element;
    }

    public static WebElement Assert_DCOrder(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.cssSelector(".product_sizes_content>div>span"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
        else if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='size_selection_container']/span[1]/span/span"));
        else element = driver.findElement(By.xpath(""));
        return element;
    }

    public static WebElement Assert_BOSS(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']/span"));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[11]"));
        else if (app == "SIX02")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='freeShippingMessage']"));
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']"));
        return element;
    }

    public static WebElement Assert_S2S(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/div[3]/span"));
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
        return element;
    }

    public static WebElement Assert_DROPSHIP(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='pdp_description']/b"));
//       else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_tabContent_description']/table/tbody/tr/td/span/b"));
        else if (app == "LADYFOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='pdp_tabContent_description']/table/tbody/tr/td/span/b"));
//        else if (app == "KIDSFOOTLOCKER")
//        else if (app == "CHAMPSSPORTS")
//        else if (app == "SIX02")
//        else if (app == "FOOTLOCKERCA")
//        else if (app == "FOOTACTION")
        return element;
    }


    public static WebElement addToCart_button(WebDriver driver) {
        if (app == "LADYFOOTLOCKER" || app == "FOOTACTION" || app == "FINAL-SCORE" || app == "FOOTACTION")
            element = driver.findElement(By.cssSelector("#addToCartLink"));
        else if (app == "CHAMPSSPORTS")
            element = driver.findElement(By.xpath(".//button[contains(@data-btnname,'addToCart')]"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.cssSelector(".active_step"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_addtocart_button']"));
        else element = driver.findElement(By.cssSelector("#pdp_addtocart_button"));
        return element;
    }

    public static WebElement StorePickup_radioButton(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_shipping_type']/div[2]/a"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.cssSelector("#dm_storepickup"));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='pdp_shipping_type']/div[2]/a"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath("//div[@id='dm_storepickup']/label[@id='lbl_storepickup']/span[contains(@class,'pdp_sprite')]"));
        //else if (app == "CHAMPSSPORTS")element = driver.findElement(By.cssSelector("#deliveryMethod_link"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='deliveryMethod_storepickup']"));
        return element;
    }

    public static WebElement Size1_select(WebDriver driver,String sizeNumber) {
        if (app == "CHAMPSSPORTS") element =  driver.findElement(By.xpath("//span/a[contains(text(),'" + sizeNumber + "')]"));
        else if (app == "FOOTLOCKERCA") element =  driver.findElement(By.xpath(".//*[@id='product_sizes']/option[contains(text(),'" + sizeNumber + "')]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_sizes']/ul/li/a[contains(text(),'" + sizeNumber + "')]"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(text(),'" + sizeNumber + "')]"));
        else element = driver.findElement(By.xpath("//a[contains(text(),'" + sizeNumber + "')]"));
        System.out.println(" The size is : "+element.getText());
        return element;
    }



    ////////////////////////////////////////////////////////////////////////////////
    //////////////////// CODE REVISION COMPLETED ABOVE THIS LINE ///////////////////
    ////////////////////////////////////////////////////////////////////////////////










    public static WebElement size_BORIS(WebDriver driver) {
        if (app == "") element = driver.findElement(By.xpath(""));
        else element = driver.findElement(By.xpath(""));
        return element;
    }



    public static WebElement size_SDD(WebDriver driver) {
        if (app == "") element = driver.findElement(By.xpath(""));
        else element = driver.findElement(By.xpath(""));
        return element;
    }

    public static WebElement size_SSI(WebDriver driver) {
        if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_2_BB3713']"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[10]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_0_42964026']"));
        else element = driver.findElement(By.xpath(""));
        return element;
    }

    public static WebElement size_Dropship(WebDriver driver) {
        if (app == "") element = driver.findElement(By.xpath(""));
        else element = driver.findElement(By.xpath(""));
        return element;
    }



    public static WebElement item1_link(WebDriver driver) {
        if (app == "EASTBAY")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/a[1]/span[1]/img"));
        else if (app == "FOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[4]/span/a[2]/img"));
        else if (app == "LADYFOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[13]/span/a[2]/img"));
        else if (app == "KIDSFOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/span/a[2]/img"));
//        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[6]/span/a[2]/img"));
        else if (app == "FOOTACTION")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[11]/span/a[2]/img"));
        else if (app == "SIX02")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/a/span[1]/img"));
//        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/a[2]/span[1]/img"));
        else if (app == "CHAMPSSPORTS")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/a[2]/span[1]/img"));
        else if (app == "FINAL-SCORE")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/span/a[2]/img"));
        else if (app == "FOOTLOCKERCA")
            element = driver.findElement(By.xpath(".//*[@id='endeca_search_results']/ul/li[1]/span/a[2]/img"));
        else element = driver.findElement(By.xpath(".//*[@id='product_image']/img"));
        return element;
    }


    public static WebElement size1_link(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[4]"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[5]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//a[contains(@id,'size_7')]"));
        else if (app == "KIDSFOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[4]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//a[contains(@id,'size_5')]"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[3]"));
        else if (app == "CHAMPSSPORTS")
            element = driver.findElement(By.xpath(".//*[@id='qv_product_92557']/div/div[5]/span[2]/div[1]/div[3]/span/a[10]"));
        else if (app == "FINAL-SCORE") element = driver.findElement(By.xpath(".//a[contains(@id,'size_5')]"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='product_sizes']/option[3]"));
        else element = driver.findElement(By.xpath(".//*[@id='size']"));
        return element;
    }





    // Added by Vrunda

    public static List<WebElement> size1link_LFL_FA(WebDriver driver) {
        List<WebElement> elements = null;
        if (app == "LADYFOOTLOCKER") elements = driver.findElements(By.xpath(".//*[@id='pdp_sizes']/ul/li"));
        else if (app == "FOOTACTION") elements = driver.findElements(By.xpath(".//*[@id='pdp_sizes']/ul/li"));
        return elements;
    }
    public static WebElement BOSS_Size1link(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(@data-sfs,'true')]"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(@data-sfs,'true')]"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(@data-sfs,'true')]"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[contains(@data-sfs,'true')]"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(@data-sfs,'true')]"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='product_sizes']/option[contains(@data-sfs,'true')]"));
        return element;
    }
    public static WebElement BOSS_TextVerify_UAT(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
//        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span[contains(@class,'sfs-message')]"));
//        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']/span"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']/span[contains(text(),'Only ships to lower 48 states.')]"));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span[contains(@class,'sfs-message')]"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[11]"));
        else if (app == "SIX02")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
        else if (app == "FOOTLOCKERCA");
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']"));
        return element;
    }
    public static WebElement BOSS_TextVerify(WebDriver driver) {
if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
//        else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span[contains(@class,'sfs-message')]"));
//        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']/span"));
else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']/span[contains(text(),'* Only ships to lower 48 states')]"));
else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span[contains(@class,'sfs-message')]"));
else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[11]"));
else if (app == "SIX02")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[2]/span"));
else if (app == "FOOTLOCKERCA");
else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='pdp_selectedSizeDisplay']"));
return element;
}

   public static WebElement SSI_Size1link(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(@data-ssi,'true')]"));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[contains(@data-ssi,'true')]"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[10]"));
        return element;
    }

    public static WebElement SSI_TextVerify(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='outer-availability-messaging']/span[1]/span/span"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/div[3]/span"));
        else if (app == "FOOTACTION")element = driver.findElement(By.xpath(".//*[@id='pdp_sizeAvailable']"));
            return element;
    }


    // the size grid container
    public static List<WebElement> Size1_grid(WebDriver driver) {
        List<WebElement> element=null;
//        if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" )
      //      element =  driver.findElements(By.xpath(".//*[@id='size_selection_container']"));
      //  element =  driver.findElements(By.xpath(".//*[@id='size_selection_container']/span[contains(@id,'size_selection_list')]"));
            //  element =  driver.findElements(By.xpath(".//*[@id='size_selection_list']/a"));

     if (app == "CHAMPSSPORTS") element =  driver.findElements(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span"));
     else if (app == "FOOTLOCKERCA") element =  driver.findElements(By.xpath(".//*[@id='product_sizes']"));
//     else if (app == "LADYFOOTLOCKER") element =  driver.findElements(By.xpath(".//*[@id='pdp_sizes']"));
     else if (app == "LADYFOOTLOCKER") element =  driver.findElements(By.xpath("//div[@id='pdp_sizes']/ul/li"));
     else if (app == "KIDSFOOTLOCKER") element = driver.findElements(By.xpath(".//*[@id='size_selection_list']/a"));
     else element =  driver.findElements(By.id("size_selection_list"));
//       else  element =  driver.findElements(By.xpath(".//*[@id='size_selection_container']"));
        return element;
    }



    public static WebElement Size(WebDriver driver) {
        if (app == "FOOTLOCKER") element =  driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[4]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_1_B24105']"));
        else if (app == "KIDSFOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[2]"));
        else if (app == "CHAMPSSPORTS") element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[3]"));
        else if (app == "SIX02") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[3]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_0_42964026']"));
        else if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[8]"));
        else if (app == "FOOTLOCKERCA") element = driver.findElement(By.xpath(".//*[@id='product_sizes']/option[1]"));


        else element = driver.findElement(By.xpath(""));
        return element;
    }

    public static WebElement BORIS_Size1link(WebDriver driver) {

        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[9]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(" "));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[3]"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[7]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_7_42964026']"));
        return element;
    }

    public static WebElement BORIS_Size(WebDriver driver) {

        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(""));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_7_42964026']"));
        return element;
    }

    public static WebElement S2S_Size1link(WebDriver driver) {
        if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[9]"));
        else if (app == "LADYFOOTLOCKER") element = driver.findElement(By.xpath(" "));
        else if (app == "KIDSFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[2]"));
        else if (app == "CHAMPSSPORTS")element = driver.findElement(By.xpath(".//*[@id='product_form']/div[1]/div[1]/div[3]/span[2]/div[1]/div[10]/div[3]/span/a[1]"));
        else if (app == "FOOTACTION") element = driver.findElement(By.xpath(".//*[@id='size_10_08317003']"));
        return element;
    }

    public static WebElement SDD_Size1link(WebDriver driver) {
        if (app == "FOOTLOCKER")
            element = driver.findElement(By.xpath(".//*[@id='size_selection_list']/a[4]"));
        return element;
    }



    public static WebElement DROPSHIP_TextVerify(WebDriver driver) {
         if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='pdp_description']/b"));
//       else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_tabContent_description']/table/tbody/tr/td/span/b"));
         else if (app == "LADYFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='pdp_tabContent_description']/table/tbody/tr/td/span/b"));
//        else if (app == "KIDSFOOTLOCKER")
//        else if (app == "CHAMPSSPORTS")
//        else if (app == "SIX02")
//        else if (app == "FOOTLOCKERCA")
//        else if (app == "FOOTACTION")
            return element;
    }



    public static WebElement DC_ORDER_TextVerify(WebDriver driver) {
        if (app == "EASTBAY") element = driver.findElement(By.xpath(".//*[@id='pdp_description']/b"));
//       else if (app == "FOOTLOCKER") element = driver.findElement(By.xpath(".//*[@id='pdp_tabContent_description']/table/tbody/tr/td/span/b"));
        else if (app == "LADYFOOTLOCKER")element = driver.findElement(By.xpath(".//*[@id='pdp_tabContent_description']/table/tbody/tr/td/span/b"));
//        else if (app == "KIDSFOOTLOCKER")
//        else if (app == "CHAMPSSPORTS")
//        else if (app == "SIX02")
//        else if (app == "FOOTLOCKERCA")
//        else if (app == "FOOTACTION")
        return element;
    }



}


