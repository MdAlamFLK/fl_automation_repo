package testapi;

import commonapi.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import pageobjects.*;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.ResultSet;

import static pageobjects.LandingPage.*;

public class SubBase extends Base {

    String sourceFile = setTestDataFile();
    public static String assertStringValue = "";


    // Search Product Numbers
    public String searchDC_ORDER_ProductNum() {
        String product_Num = null;
        try {
            if (app == "EASTBAY") product_Num = readFromExcel(sourceFile, "Product_SKU", "E2");
            else if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "E3");
            else if (app == "LADYFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "E4");
            else if (app == "KIDSFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "E5");
            else if (app == "CHAMPSSPORTS") product_Num = readFromExcel(sourceFile, "Product_SKU", "E6");
            else if (app == "SIX02") product_Num = readFromExcel(sourceFile, "Product_SKU", "E7");
            else if (app == "FOOTACTION") product_Num = readFromExcel(sourceFile, "Product_SKU", "E9");
            else if (app == "FINAL-SCORE") product_Num = readFromExcel(sourceFile, "Product_SKU", "E10");
        } catch (Exception e) {
            log.info("Unable to retrive the DC_ORDER product Number");
        }
        return product_Num;
    }

    public String searchBOSS_ProductNum() {
        String product_Num = null;
        try {
            if (app == "EASTBAY") product_Num = readFromExcel(sourceFile, "Product_SKU", "B2");
            else if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "B3");
            else if (app == "LADYFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "B4");
            else if (app == "KIDSFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "B5");
            else if (app == "CHAMPSSPORTS") product_Num = readFromExcel(sourceFile, "Product_SKU", "B6");
            else if (app == "SIX02") product_Num = readFromExcel(sourceFile, "Product_SKU", "B7");
            else if (app == "FOOTLOCKERCA") product_Num = readFromExcel(sourceFile, "Product_SKU", "B8");
            else if (app == "FOOTACTION") product_Num = readFromExcel(sourceFile, "Product_SKU", "B9");
        } catch (Exception e) {
            log.info("Unable to retrive the BOSS product Number");
        }
        return product_Num;
    }

    public String searchS2S_ProductNum() {
        String product_Num = null;
        try {
            if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "C3");
            else if (app == "LADYFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "C4");
            else if (app == "KIDSFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "C5");
            else if (app == "CHAMPSSPORTS") product_Num = readFromExcel(sourceFile, "Product_SKU", "C6");
            else if (app == "FOOTACTION") product_Num = readFromExcel(sourceFile, "Product_SKU", "C9");
        } catch (Exception e) {
            log.info("Unable to retrive the Send To Store product Number");
        }
        return product_Num;
    }

    public String searchBORIS_ProductNum() {
        String product_Num = null;
        try {
            if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "D3");
            else if (app == "LADYFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "D4");
            else if (app == "KIDSFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "D5");
            else if (app == "CHAMPSSPORTS") product_Num = readFromExcel(sourceFile, "Product_SKU", "D6");
            else if (app == "FOOTACTION") product_Num = readFromExcel(sourceFile, "Product_SKU", "D9");
        } catch (Exception e) {
            log.info("Unable to retrive the BORIS product Number");
        }
        return product_Num;
    }

    public String searchSDD_ProductNum() {
        String product_Num = null;
        try {
            if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "F3");
            typeAfterElementPresent(search_Textbox(driver), product_Num);
        } catch (Exception e) {
            log.info("Unable to retrive the Same Day Delivery product Number");
        }
        return product_Num;
    }

    public String searchSSI_ProductNum() {
        String product_Num = null;
        try {
            if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "G3");
            else if (app == "LADYFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "G4");
            else if (app == "KIDSFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "G5");
            else if (app == "CHAMPSSPORTS") product_Num = readFromExcel(sourceFile, "Product_SKU", "G6");
            else if (app == "FOOTACTION") product_Num = readFromExcel(sourceFile, "Product_SKU", "G9");
        } catch (Exception e) {
            log.info("Unable to retrive the SSI product Number");
        }
        return product_Num;
    }

    public String searchDROPSHIP_ProductNum() {
        String product_Num = null;
        try {
            if (app == "EASTBAY") product_Num = readFromExcel(sourceFile, "Product_SKU", "H2");
            else if (app == "FOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "H3");
            else if (app == "LADYFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "H4");
            else if (app == "KIDSFOOTLOCKER") product_Num = readFromExcel(sourceFile, "Product_SKU", "H5");
            else if (app == "CHAMPSSPORTS") product_Num = readFromExcel(sourceFile, "Product_SKU", "H6");
            else if (app == "SIX02") product_Num = readFromExcel(sourceFile, "Product_SKU", "H7");
            else if (app == "FOOTLOCKERCA") product_Num = readFromExcel(sourceFile, "Product_SKU", "H8");
            else if (app == "FOOTACTION") product_Num = readFromExcel(sourceFile, "Product_SKU", "H9");
//            typeAfterElementPresent(search_Textbox(driver), product_Num);
        } catch (Exception e) {
            log.info("Unable to retrive the DROPSHIP product Number");
        }
        return product_Num;
    }


    // Search Product Sizes
    public String searchDC_ORDER_Size() {
        String size = null;
        try {
            if (app == "EASTBAY") size = readFromExcel(sourceFile, "Product_Size", "E2");
            else if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "E3");
            else if (app == "LADYFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "E4");
            else if (app == "KIDSFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "E5");
            else if (app == "CHAMPSSPORTS") size = readFromExcel(sourceFile, "Product_Size", "E6");
            else if (app == "SIX02") size = readFromExcel(sourceFile, "Product_Size", "E7");
            else if (app == "FOOTACTION") size = readFromExcel(sourceFile, "Product_Size", "E9");
            else if (app == "FINAL-SCORE") size = readFromExcel(sourceFile, "Product_Size", "E10");
        } catch (Exception e) {
            log.info("Unable to retrive the DC_ORDER size ");
        }
        return size;
    }

    public String searchBOSS_Size() {
        String size = null;
        try {
            if (app == "EASTBAY") size = readFromExcel(sourceFile, "Product_Size", "B2");
            else if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "B3");
            else if (app == "LADYFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "B4");
            else if (app == "KIDSFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "B5");
            else if (app == "CHAMPSSPORTS") size = readFromExcel(sourceFile, "Product_Size", "B6");
            else if (app == "SIX02") size = readFromExcel(sourceFile, "Product_Size", "B7");
            else if (app == "FOOTLOCKERCA") size = readFromExcel(sourceFile, "Product_Size", "B8");
            else if (app == "FOOTACTION") size = readFromExcel(sourceFile, "Product_Size", "B9");
//            clickAfterElementPresent_UAT(ShopPage.Size1_select(driver, size));
//            System.out.println("Size selected " + size);
        } catch (Exception e) {
            log.info("Unable to retrive the BOSS size");
        }
        return size;
    }

    public String searchS2S_Size() {
        String size = null;
        try {
            if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "C3");
            else if (app == "LADYFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "C4");
            else if (app == "KIDSFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "C5");
            else if (app == "CHAMPSSPORTS") size = readFromExcel(sourceFile, "Product_Size", "C6");
            else if (app == "FOOTACTION") size = readFromExcel(sourceFile, "Product_Size", "C9");
        } catch (Exception e) {
            log.info("Unable to retrive the S2S size");
        }
        return size;
    }

    public String searchBORIS_Size() throws InterruptedException {
        String size = null;
        Thread.sleep(oneSec);
        try {
            if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "D3");
            else if (app == "LADYFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "D4");
            else if (app == "KIDSFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "D5");
            else if (app == "CHAMPSSPORTS") size = readFromExcel(sourceFile, "Product_Size", "D6");
            else if (app == "FOOTACTION") size = readFromExcel(sourceFile, "Product_Size", "D9");
        } catch (Exception e) {
            log.info("Unable to retrive the BORIS size");
        }
        Thread.sleep(oneSec);
        return size;
    }

    public String searchSDD_Size() {
        String size = null;
        try {
            if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "F3");
        } catch (Exception e) {
            log.info("Unable to retrive the Same Day Delivery size ");
        }
        return size;
    }

    public String searchSSI_Size() {
        String size = null;
        try {
            if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "G3");
            else if (app == "LADYFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "G4");
            else if (app == "KIDSFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "G5");
            else if (app == "CHAMPSSPORTS") size = readFromExcel(sourceFile, "Product_Size", "G6");
            else if (app == "FOOTACTION") size = readFromExcel(sourceFile, "Product_Size", "G9");
            //getSizeFromExcelSheet(size);
        } catch (Exception e) {
            log.info("Unable to retrive the SSI size");
        }
        return size;
    }

    public String searchDROPSHIP_Size() {
        String size = null;
        try {
            if (app == "EASTBAY") size = readFromExcel(sourceFile, "Product_Size", "H2");
            else if (app == "FOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "H3");
            else if (app == "LADYFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "H4");
            else if (app == "KIDSFOOTLOCKER") size = readFromExcel(sourceFile, "Product_Size", "H5");
            else if (app == "CHAMPSSPORTS") size = readFromExcel(sourceFile, "Product_Size", "H6");
            else if (app == "SIX02") size = readFromExcel(sourceFile, "Product_Size", "H7");
            else if (app == "FOOTLOCKERCA") size = readFromExcel(sourceFile, "Product_Size", "H8");
            else if (app == "FOOTACTION") size = readFromExcel(sourceFile, "Product_Size", "H9");
            System.out.println("Size selected " + size);
        } catch (Exception e) {
            log.info("Unable to retrive the BOSS size");
        }
        return size;
    }


    // Search Product Zipcodes
    public void searchDC_ORDER_Zipcode() {}

    public void searchBOSS_Zipcode() {}

    public String searchS2S_Zipcode() throws InterruptedException {
        String zipcode = null;
        try {
            if (app == "FOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "C3");
            else if (app == "LADYFOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "C4");
            else if (app == "KIDSFOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "C5");
            else if (app == "CHAMPSSPORTS") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "C6");
            else if (app == "FOOTACTION") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "C9");
        } catch (Exception e) {
            log.info("Unable to retrive the Send To Store zipcode");
        }
        return zipcode;
    }

    public String searchBORIS_Zipcode() throws InterruptedException {
        String zipcode = null;
        try {
            if (app == "FOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "D3");
            else if (app == "LADYFOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "D4");
            else if (app == "KIDSFOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "D5");
            else if (app == "CHAMPSSPORTS") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "D6");
            else if (app == "FOOTACTION") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "D9");
        } catch (Exception e) {
            log.info("Unable to retrive the BORIS zipcode");
        }
        return zipcode;
    }

    public String searchSDD_Zipcode() {
        String zipcode = null;
        try {
            if (app == "FOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "F3");
        } catch (Exception e) {
            log.info("Unable to retrive the Same Day Delivery zipcode");
        }
        return zipcode;
    }

    public String searchSSI_Zipcode() {
        String zipcode = null;
        try {
            if (app == "FOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "G3");
            else if (app == "LADYFOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "G4");
            else if (app == "KIDSFOOTLOCKER") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "G5");
            else if (app == "CHAMPSSPORTS") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "G6");
            else if (app == "FOOTACTION") zipcode = readFromExcel(sourceFile, "Product_Zipcode", "G9");
            System.out.println(" Zipcode input is :" + zipcode);
        } catch (Exception e) {
            log.info("Unable to retrive the SSI zipcode");
        }
        return zipcode;
    }

    public void searchDROPSHIP_Zipcode() {}


    // Close PopUps
    public void closeEmailSignUp_popUp1() throws InterruptedException {
        Thread.sleep(shortWait);
        try {
            WebElement closeEmailSignUp_popUp1 = driver.findElement(By.xpath("//*[@id=\"email_form_overlay\"]/iframe"));
            if (closeEmailSignUp_popUp1.isDisplayed()) {
                driver.switchTo().frame(closeEmailSignUp_popUp1);
                driver.findElement(By.id("overlay_header_close_button")).click();
                driver.switchTo().defaultContent();
            }
        } catch (Exception e) {
            log.info("EMAIL SIGNUP POPUP_1 NOT FOUND  ");
        }
        Thread.sleep(shortWait);
    }

    public void closeEmailSignUp_popUp2() throws InterruptedException {
        Thread.sleep(shortWait);
        try {
            WebElement closeEmailSignUp_popUp2 = driver.findElement(By.xpath("/html/body"));
            if (closeEmailSignUp_popUp2.isDisplayed()) {
                //driver.switchTo().frame(closeEmailSignUp_popUp2);
                //driver.findElement(By.xpath("//*[@id='overlay_header_close_button']")).click();
                clickAfterElementPresent(driver.findElement(By.xpath("//*[@id='overlay_header_close_button']")));
                //driver.switchTo().defaultContent();
            }
        } catch (Exception e) {
            log.info("EMAIL SIGNUP POPUP_2 NOT FOUND ");
        }
        Thread.sleep(shortWait);
    }

    public void closePopUp_1() throws InterruptedException {
        Thread.sleep(shortWait);
        try {
            WebElement popUp_1 = driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[3]/div[1]/div[1]/div[1]/span/a/span"));
            if (popUp_1.isDisplayed()) {
                driver.findElement(By.xpath("/html/body/div[2]/div[3]/div[3]/div[1]/div[1]/div[2]/a/span")).click();
            }
        } catch (Exception e) {
            log.info("Pop Up 1 not found ");
        }
        Thread.sleep(shortWait);
    }

    public void closeFLCAOverlay_PopUp() throws InterruptedException {
        Thread.sleep(shortWait);
        try {
            WebElement closeFLCAOverlay_PopUp = driver.findElement(By.xpath(".//*[@id='modal_close_x']/a/span"));
            if (closeFLCAOverlay_PopUp.isDisplayed()) {
                driver.findElement(By.xpath(".//*[@id='modal_close_x']/a/span")).click();
            }
        } catch (Exception e) {
            log.info("FLCAOverlay_PopUp not found ");
        }
        Thread.sleep(shortWait);
    }

    public void closePopUp_3() {
        try {
            WebElement popUp_3 = driver.findElement(By.xpath("html/body/div[6]/div[2]/div[1]/div[1]"));
            if (popUp_3.isDisplayed()) {
                popUp_3.click();
                System.out.println("PopUp3 window closed");
            }

        } catch (Exception e) {
            log.info("Pop Up 3 not found ");
        }
    }


    // Application Functions
    public String setTestDataFile() {
        if (env == "UAT")
            return getPropertyValue("sourcefile_UAT");
            //return "./src/main/resources/TestData_UAT.xlsx";
        else
            return getPropertyValue("sourcefile_PROD");
        //return "./src/main/resources/TestData_PROD.xlsx";
    }

    public void login() throws InterruptedException {
        try {
            String userName = readFromExcel(sourceFile, "Credential", "B2");
            String password = readFromExcel(sourceFile, "Credential", "C2");
            clickAfterElementPresent(HomePage.user_icon(driver));
            typeAfterElementPresent(HomePage.userId_field(driver), userName);
            typeAfterElementPresent(HomePage.password_field(driver), password);
            clickAfterElementPresent(HomePage.login_button(driver));
        } catch (Exception e) {
            log.info("Unable to login :(");
        }
        Thread.sleep(shortWait);
    }

    public void addToCart() {
        try {
            clickAfterElementPresent(shop_link(driver));
            System.out.println(" shop link clicked" + shop_link(driver));
            clickAfterElementPresent(category_link(driver));
            if (app.contains("_M")) {
                if (app == "EASTBAY_M" || app == "FOOTLOCKER_M" || app == "KIDSFOOTLOCKER_M") {
                    clickAfterElementPresent(section_link(driver));
                    clickAfterElementPresent(option_link(driver));
                } else {
                    clickAfterElementPresent(option_link(driver));
                }
                clickAfterElementPresent(ShopPage.item1_link(driver));
                driver.navigate().refresh();
                Thread.sleep(oneSec);
                selectFromDropdown(ShopPage.size1_link(driver), 5);
            } else {
                clickAfterElementPresent(option_link(driver));
                clickAfterElementPresent(ShopPage.item1_link(driver));
                clickAfterElementPresent(ShopPage.size_link(driver));
                clickAfterElementPresent(ShopPage.size1_link(driver));
            }
            clickAfterElementPresent(ShopPage.addToCart_button(driver));
            Thread.sleep(oneSec);
        } catch (Exception e) {
            log.info("Unable to add the item to cart :(");
        }
    }

    public void checkout() {
        try {
            clickAfterElementPresent(HomePage.viewCartAndCheckout_button(driver));
            clickAfterElementPresent(HomePage.cartCheckout_button(driver));
        } catch (Exception e) {
        }
    }

    public void setAddressForm() {
        try {
            String billFirstName = readFromExcel(sourceFile, "Billing_Details", "B2");
            String billLastName = readFromExcel(sourceFile, "Billing_Details", "C2");
            String billStreet = readFromExcel(sourceFile, "Billing_Details", "D2");
            String billApt = readFromExcel(sourceFile, "Billing_Details", "E2");
            String billZipCode = readFromExcel(sourceFile, "Billing_Details", "F2");
            String billPhone = readFromExcel(sourceFile, "Billing_Details", "I2");
            String billMail = readFromExcel(sourceFile, "Billing_Details", "J2");
            String shipFirstName = readFromExcel(sourceFile, "Shipping_Details", "B2");
            String shipLastName = readFromExcel(sourceFile, "Shipping_Details", "C2");
            String shipStreet = readFromExcel(sourceFile, "Shipping_Details", "D2");
            String shipApt = readFromExcel(sourceFile, "Shipping_Details", "E2");
            String shipZipCode = readFromExcel(sourceFile, "Shipping_Details", "F2");
            String shipPhone = readFromExcel(sourceFile, "Shipping_Details", "I2");
            typeAfterElementPresent(PurchasePage.billFirstName_field(driver), billFirstName);
            typeAfterElementPresent(PurchasePage.billLastName_field(driver), billLastName);
            typeAfterElementPresent(PurchasePage.billStreet_field(driver), billStreet);
            typeAfterElementPresent(PurchasePage.billApt_field(driver), billApt);
            typeAfterElementPresent(PurchasePage.billZipCode_field(driver), billZipCode);
            typeAfterElementPresent(PurchasePage.billPhone_field(driver), billPhone);
            Thread.sleep(oneSec);
            typeAfterElementPresent(PurchasePage.billEmail_field(driver), billMail);
            Thread.sleep(oneSec);
            clickAfterElementPresent(PurchasePage.shipTo_checkbox(driver));
            typeAfterElementPresent(PurchasePage.shipFirstName_field(driver), shipFirstName);
            typeAfterElementPresent(PurchasePage.shipLastName_field(driver), shipLastName);
            typeAfterElementPresent(PurchasePage.shipStreet_field(driver), shipStreet);
            typeAfterElementPresent(PurchasePage.shipApt_field(driver), shipApt);
            typeAfterElementPresent(PurchasePage.shipZipCode_field(driver), shipZipCode);
            typeAfterElementPresent(PurchasePage.shipPhone_field(driver), shipPhone);
            Thread.sleep(oneSec);
            clickAfterElementPresent(PurchasePage.shipPaneContinue_button(driver));
        } catch (Exception e) {
            log.info("Unable to fill the address form :(");
        }
    }

    public void setBillingAddress() {
        try {
            String sheetRef = "Billing_Details";
            if (app == "FOOTLOCKERCA") sheetRef = "Billing_Details_FLCA";
            String billFirstName = readFromExcel(sourceFile, sheetRef, "B2");
            String billLastName = readFromExcel(sourceFile, sheetRef, "C2");
            String billStreet = readFromExcel(sourceFile, sheetRef, "D2");
            String billApt = readFromExcel(sourceFile, sheetRef, "E2");
            String billZipCode = readFromExcel(sourceFile, sheetRef, "F2");
            String billCity = readFromExcel(sourceFile, sheetRef, "G2");
            String billPhone = readFromExcel(sourceFile, sheetRef, "I2");
            String billMail = readFromExcel(sourceFile, sheetRef, "J2");
            typeAfterElementPresent(PurchasePage.billFirstName_field(driver), billFirstName);
            typeAfterElementPresent(PurchasePage.billLastName_field(driver), billLastName);
            typeAfterElementPresent(PurchasePage.billStreet_field(driver), billStreet);
            typeAfterElementPresent(PurchasePage.billApt_field(driver), billApt);
            typeAfterElementPresent(PurchasePage.billZipCode_field(driver), billZipCode);
            if (app == "FOOTLOCKERCA") typeAfterElementPresent(PurchasePage.billCity_field(driver), billCity);
            typeAfterElementPresent(PurchasePage.billPhone_field(driver), billPhone);
            Thread.sleep(oneSec);
            typeAfterElementPresent(PurchasePage.billEmail_field(driver), billMail);
            Thread.sleep(oneSec);
            clickAfterElementPresent(PurchasePage.billPaneContinue_button(driver));
        } catch (Exception e) {
            log.info("Unable to fill the billing address form :(");
        }
    }

    public void setShippingAddress() {
        try {
            clickAfterElementPresent(driver.findElement(By.cssSelector("#shipPaneEdit")));
            String shipFirstName = readFromExcel(sourceFile, "Shipping_Details", "B2");
            String shipLastName = readFromExcel(sourceFile, "Shipping_Details", "C2");
            String shipStreet = readFromExcel(sourceFile, "Shipping_Details", "D2");
            String shipApt = readFromExcel(sourceFile, "Shipping_Details", "E2");
            String shipZipCode = readFromExcel(sourceFile, "Shipping_Details", "F2");
            String shipPhone = readFromExcel(sourceFile, "Shipping_Details", "I2");
            Thread.sleep(oneSec);
            typeAfterElementPresent(PurchasePage.shipFirstName_field(driver), shipFirstName);
            typeAfterElementPresent(PurchasePage.shipLastName_field(driver), shipLastName);
            typeAfterElementPresent(PurchasePage.shipStreet_field(driver), shipStreet);
            typeAfterElementPresent(PurchasePage.shipApt_field(driver), shipApt);
            typeAfterElementPresent(PurchasePage.shipZipCode_field(driver), shipZipCode);
            typeAfterElementPresent(PurchasePage.shipPhone_field(driver), shipPhone);
            Thread.sleep(oneSec);
            clickAfterElementPresent(PurchasePage.shipPaneContinue_button(driver));
        } catch (Exception e) {
            log.info("Unable to fill the shipping address form :(");
        }
    }

    public void setDeliveryOptions() {
        try {
            clickAfterElementPresent(PurchasePage.shipMethodPaneContinue_button(driver));
        } catch (Exception e) {
            log.info("Unable to set the delivery options :(");
        }
    }

    public void setPayment() {
        try {
            String cardNumber = readFromExcel(sourceFile, "Credit_Cards", "D2");
            String cardExpireDateMM = readFromExcel(sourceFile, "Credit_Cards", "E2");
            String cardExpireDateYY = readFromExcel(sourceFile, "Credit_Cards", "F2");
            String cardCCV = readFromExcel(sourceFile, "Credit_Cards", "G2");
            typeAfterElementPresent(PurchasePage.cardNumber_field(driver), cardNumber);
            typeAfterElementPresent(PurchasePage.cardExpireDateMM_field(driver), cardExpireDateMM);
            typeAfterElementPresent(PurchasePage.cardExpireDateYY_field(driver), cardExpireDateYY);
            typeAfterElementPresent(PurchasePage.cardCCV_field(driver), cardCCV);
            clickAfterElementPresent(PurchasePage.payMethodPaneContinue_button(driver));
            Thread.sleep(oneSec);
        } catch (Exception e) {
            log.info("Unable to make the payment :(");
        }
    }

    public void submitOrder() throws InterruptedException {
        try {
            clickAfterElementPresent(PurchasePage.orderSubmit_label(driver));
            Thread.sleep(oneSec);
            System.out.println("------------------------------< ORDER SUCCESSFULLY SUBMITTED IN " + app + " >----------------------------------");
        } catch (Exception e) {
            log.info("Unable to submit the order :(");
        }
        log.info("submitOrder: " + driver.getTitle());
        Thread.sleep(shortWait);
    }




    public String getTextFromElementContainingStringThenClick(String initText) {
        String text = "";
        List<WebElement> elements = ISAPage.locationList_grid(driver);
        for (WebElement e : elements) {
            try {
//                if ((e.getText().toUpperCase().contains("PAY NOW, SHIP TO STORE"))) {
                if ((e.getText().toUpperCase().contains(initText))) {
                    text = e.getText().toUpperCase();
                    String elementId = e.getAttribute("id");

                    clickAfterElementPresent(ISAPage.S2SpayNow_button(driver, elementId));
//                    clickAfterElementPresent(ISAPage.BORISpayNow_button(driver, elementId));
                    return text;
                }
            } catch (Exception objException) {
                objException.printStackTrace();
            }
        }
        return "TEXT NOT FOUND";
    }


    public String searchStoreButtonContaining(String initText) {
        String elementId = null;
        List<WebElement> elements = ISAPage.locationList_grid(driver);
        for (WebElement e : elements) {
            try {
                if ((e.getText().toUpperCase().contains(initText))) {
                    assertStringValue = e.getText().toUpperCase();
                    elementId = e.getAttribute("id");
                }
            } catch (Exception objException) {
                objException.printStackTrace();
            }
        }
        return elementId;
    }



    public void getSizeFromExcelSheet(String size) throws Exception {
        List<String> strList = new ArrayList<String>();
        List<WebElement> elements = ShopPage.Size1_grid(driver);
        String strCell = size;
        for (int i = 0; i < elements.size(); i++) {
            String[] strArray = elements.get(i).getText().split("\\.0*$");
            int index = 2;
            for (int k = 2; k <= strArray[0].length(); k += 4) {
                if (index >= strArray[0].length()) {
                    index = strArray[0].length() - 2;
                    String strSub = strArray[0].substring(index, strArray[0].length());
                    strList.add(strSub);
                } else {
                    String strSub = strArray[0].substring(index - 2, index + 2);
                    strList.add(strSub);
                    index = index + 4;
                }
            }
            for (String s : strList) {
                if (s.equals(strCell)) {
                    clickAfterElementPresent(ShopPage.Size1_select(driver, strCell));
                    Thread.sleep(1000);
                }
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////////////
    //////////////////// CODE REVISION COMPLETED ABOVE THIS LINE ///////////////////
    ////////////////////////////////////////////////////////////////////////////////



    public void switchToDefaultPage() {
        try {
            driver.switchTo().defaultContent();
            System.out.println("Switched back to webpage from frame");
        } catch (Exception e) {
            System.out.println("Unable to switch back to the webpage");
        }
    }

    public void closeLastPg_PopUp() {
        try {
            WebElement closeLastPg_PopUp = driver.findElement(By.xpath("/html/body/div[2]"));
            if (closeLastPg_PopUp.isDisplayed()) {
                driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[1]")).click();
                System.out.println("LastPg_PopUp window closed");

            }

        } catch (Exception e) {
            log.info("LastPg_PopUp not found ");
        }
    }

    public void addToCart_UAT1() {
        try {
            closePopUp_1();
            closeEmailSignUp_popUp1();
            Thread.sleep(oneSec);
            clickAfterElementPresent(shop_link(driver));
            System.out.println("Shop link clicked_1");
            Thread.sleep(oneSec);
            clickAfterElementPresent(category_link(driver));
            System.out.println("category_link  clicked_2");
            Thread.sleep(oneSec);
            if (app.contains("_M")) {
                if (app == "EASTBAY_M" || app == "FOOTLOCKER_M" || app == "KIDSFOOTLOCKER_M") {
                    clickAfterElementPresent(section_link(driver));
                    clickAfterElementPresent(option_link(driver));
                } else {
                    clickAfterElementPresent(option_link(driver));
                }
                Thread.sleep(oneSec);
                clickAfterElementPresent(ShopPage.item1_link(driver));
                driver.navigate().refresh();
                Thread.sleep(oneSec);
                selectFromDropdown(ShopPage.size1_link(driver), 5);
                Thread.sleep(oneSec);

            } else {
                clickAfterElementPresent(option_link(driver));
                System.out.println("option_link  clicked_3");
                Thread.sleep(oneSec);
                closeEmailSignUp_popUp1();
                Thread.sleep(oneSec);
                clickAfterElementPresent(ShopPage.item1_link(driver));
                System.out.println("ShopPage.item1_link  clicked_4");
                Thread.sleep(oneSec);
                Thread.sleep(oneSec);

                clickAfterElementPresent(ShopPage.size_link(driver));
                Thread.sleep(oneSec);
                Thread.sleep(oneSec);
                System.out.println("ShopPage.size_link  clicked_5");
                Thread.sleep(oneSec);
                clickAfterElementPresent(ShopPage.size1_link(driver));
                System.out.println("ShopPage.size1_link  clicked_6");
                Thread.sleep(oneSec);

            }
            clickAfterElementPresent(ShopPage.addToCart_button(driver));
            System.out.println("ShopPage.addtoCart_button  clicked_7");
            Thread.sleep(oneSec);
        } catch (Exception e) {
            log.info("Unable to add the item to cart :(");
        }
        log.info("Item added to the cart successfully :)");
    }

    public void checkout_UAT() {
        try {

            if (app == "KIDSFOOTLOCKER") clickUsingJavascriptExecutor(HomePage.viewCartAndCheckout_button(driver));
            else clickAfterElementPresent(HomePage.viewCartAndCheckout_button(driver));
            clickAfterElementPresent(HomePage.cartCheckout_button(driver));
        } catch (Exception e) {
            log.info("Unable to checkout :(");
        }
        log.info("Checked out successfully :)");
    }

    public void setBillingAddress_SDD() {
        try {
            String billFirstName = readFromExcel(sourceFile, "Billing_Details_SDD", "B2");
            String billLastName = readFromExcel(sourceFile, "Billing_Details_SDD", "C2");
            String billStreet = readFromExcel(sourceFile, "Billing_Details_SDD", "D2");
            String billApt = readFromExcel(sourceFile, "Billing_Details_SDD", "E2");
            String billZipCode = readFromExcel(sourceFile, "Billing_Details_SDD", "F2");
            String billPhone = readFromExcel(sourceFile, "Billing_Details_SDD", "I2");
            String billMail = readFromExcel(sourceFile, "Billing_Details_SDD", "J2");
            typeAfterElementPresent(PurchasePage.billFirstName_field(driver), billFirstName);
            typeAfterElementPresent(PurchasePage.billLastName_field(driver), billLastName);
            typeAfterElementPresent(PurchasePage.billStreet_field(driver), billStreet);
            typeAfterElementPresent(PurchasePage.billApt_field(driver), billApt);
            typeAfterElementPresent(PurchasePage.billZipCode_field(driver), billZipCode);
            typeAfterElementPresent(PurchasePage.billPhone_field(driver), billPhone);
            Thread.sleep(oneSec);
            typeAfterElementPresent(PurchasePage.billEmail_field(driver), billMail);
            Thread.sleep(oneSec);
            clickAfterElementPresent(PurchasePage.billPaneContinue_button(driver));
        } catch (Exception e) {
            log.info("Unable to fill the billing address form :(");
        }
    }

    public void submitOrder_UAT() {
        try {
            clickAfterElementPresent(PurchasePage.orderSubmit_label(driver));
            Thread.sleep(oneSec);
            System.out.println("Order Number is :   " + orderNumber());
            System.out.println("Customer Number is :   " + customerNumber());
            closeLastPg_PopUp();//added by Vrunda
            assertStringValue = "Order Number";
            System.out.println(assertStringValue + " in " + app);
        } catch (Exception e) {
            log.info("Unable to submit the order :(");
        }
        log.info("submitOrder: " + driver.getTitle());
        Assert.assertEquals(assertStringValue, "Order Number");
    }

    public String orderNumber() {
        String orderNum = "null";
        try {
            orderNum = driver.findElement(By.xpath("//*[@id='confirmationNumber']")).getText();
        } catch (Exception e) {
            log.info("Order Number Not Found ");
        }
        return (orderNum);
    }

    public String customerNumber() {
        String custNum = "null";
        try {
            custNum = driver.findElement(By.xpath("/*//*[@id='orderReceipt']/div[1]/div/p[3]/span")).getText();
        } catch (Exception e) {
            log.info("Customer Number Not Found ");
        }
        return (custNum);
    }

    public String searchProduct_Zipcode(String Brand, String fullfillmentType) {
        String zip = null;
        try {
            zip = getXLSXTestData(sourceFile, "Product_Zipcode", Brand, fullfillmentType);
            System.out.println("brand name :" + Brand + "Fullfillment Type" + fullfillmentType);

        } catch (Exception e) {
            log.info("Unable to retrive the  Zipcode");
        }
        return zip;
    }

    public String searchProduct_Num(String Brand, String fullfillmentType) {
        String Product_Num = null;
        try {
            Product_Num = getXLSXTestData(sourceFile, "Product_SKU", Brand, fullfillmentType);
            System.out.println("brand name :" + Brand + "Fullfillment Type" + fullfillmentType);

        } catch (Exception e) {
            log.info("Unable to retrive the  product number");
        }
        return Product_Num;
    }


//    public static void retrievData() throws InterruptedException, SQLException {
//    String productName = "";
//    String sql = "select DESCRIPTION from QE1_CORE_OWNER.ACTUAL_PRODUCT_PACK where PRODUCT_FULL_NAME = '";
//    ResultSet rs = Base.getResult(sql);
//		rs.next();
//        productName = rs.getString("DESCRIPTION");
//    }


    public void checkoutForFullfillmentType() {
        try {
            clickAfterElementPresent(HomePage.viewCartAndCheckout_button(driver));
            clickAfterElementPresent(HomePage.cartCheckout_button(driver));
            setBillingAddress();
            clickAfterElementPresent(PurchasePage.shipMethodPaneContinue_button(driver));
            assertStringValue = getTextFromLabelAfterElementPresent(HomePage.promoCode_label(driver)).toUpperCase();
            System.out.println(" assertStringValue " + assertStringValue);
        } catch (Exception e) {
            log.info("Unable to checkout :(");
        }
        Assert.assertEquals(assertStringValue, "3. PROMO CODE (OPTIONAL)");
        log.info("Checked out successfully :)");
    }

    public void verifyBOSS() {
        try {
            driver.manage().window().maximize();
            if (app == "EASTBAY" || app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FOOTLOCKERCA" || app == "FOOTACTION") {
                clickAfterElementPresent(LandingPage.search_link(driver));
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchBOSS_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                if (app == "EASTBAY" || app == "FOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FOOTLOCKERCA") {
                    clickAfterElementPresent(ShopPage.size_link(driver));
                }
                System.out.println("Size will be  selected");
                searchBOSS_Size();
                System.out.println("Size clicked");
                Thread.sleep(oneSec);
                System.out.println(ShopPage.BOSS_TextVerify(driver).getText());
                if (app == "FOOTLOCKERCA") {
                    System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                } else {
                    try {
                        Thread.sleep(oneSec);
                        String BossTextString = ShopPage.BOSS_TextVerify(driver).getText().toUpperCase();
                        if (BossTextString.contains("SHIPS TO THE 48 CONTIGUOUS UNITED STATES") || BossTextString.contains("* ONLY SHIPS TO LOWER 48 STATES") || BossTextString.contains("ONLY SHIPS TO LOWER 48 STATES. NO GUARANTEED FOR CHRISTMAS.")) {
                            System.out.println(ShopPage.BOSS_TextVerify(driver).getText());
                            System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                        } else System.out.println(" UNABLE TO VERIFY THE TEXT FOR THE PRODUCT. ");
                    } catch (Exception e1) {
                        log.info("UNABLE TO VERIFY THE TEXT: '* Only ships to lower 48 states' FOR BOSS FULLFILLMENT TYPE");
                    }
                }
                try {
                    clickAfterElementPresent(ShopPage.addToCart_button(driver));
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                checkout();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                System.out.println(" Order is submitted ");
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT BOSS FULLFILLMENT TYPE");
            }
        } catch (
                Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR BOSS FULLFILLMENT TYPE");
        }
    }

    public void verifySingleStoreInventory() {
        try {
            driver.manage().window().maximize();
            if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "FOOTACTION") {
                clickAfterElementPresent(LandingPage.search_link(driver));
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                searchSSI_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                if (app == "LADYFOOTLOCKER" || app == "FOOTACTION") {
                    List<WebElement> elements = ShopPage.size1link_LFL_FA(driver);
                    for (WebElement e : elements) {
                        System.out.println(e.getText());
                        e.click();
                        if (ShopPage.SSI_TextVerify(driver).getText().contains("AVAILABLE FOR PURCHASE IN SELECT STORES") || ShopPage.SSI_TextVerify(driver).getText().contains("Available for purchase in select stores")) {
                            System.out.println(ShopPage.SSI_TextVerify(driver).getText());
                            break;
                        } else
                            System.out.println("UNABLE TO VERIFY THE TEXT: 'AVAILABLE FOR PURCHASE IN SELECT STORES' FOR SINGLE STORE INVENTORY FULLFILLMENT TYPE");
                    }
                } else {
                    Thread.sleep(1000);
                    clickAfterElementPresent(ShopPage.size_link(driver));
                    Thread.sleep(1000);
                    clickAfterElementPresent(ShopPage.SSI_Size1link(driver));
                    System.out.println(ShopPage.SSI_Size1link(driver).getText());
                    Thread.sleep(1000);
                    System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A SSI PRODUCT ");
                }

                Thread.sleep(1000);
                clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
                searchSSI_Zipcode();
                clickAfterElementPresent(ISAPage.findStores_button(driver));
                List<WebElement> elements = ISAPage.locationList_grid(driver);
                for (WebElement e : elements) {
                    int j = 1;
                    System.out.println("=========================");
                    System.out.println(" STORE LOCATION : " + j);
                    System.out.println(e.getText());
                    Thread.sleep(oneSec);
                    j++;
                }
                System.out.println("=========================");
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT SSI FULLFILLMENT TYPE");
            }

        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR SINGLE STORE INVENTORY FULLFILLMENT TYPE");

        }
    }

    public void verifyPickUpOptions() {
        String pickupTodayLabel = PurchasePage.BORIS_PickupTodayRadioButtonVerify(driver).getText();
        String pickupTodayRadioButton = PurchasePage.BORIS_StorePickupTodayLabelVerify(driver).getText();
        System.out.println("pickupTodayLabel : " + pickupTodayLabel);
        System.out.println("pickupTodayRadioButton : " + pickupTodayRadioButton);

        if (((pickupTodayLabel.toUpperCase().contains("STORE PICKUP TODAY")) && (pickupTodayRadioButton.toUpperCase().contains("TODAY")))) {
            System.out.println("pickup options match : ");
        } else System.out.println("pickup options do not match : ");
    }

    public void verifyBORIS() {
        try {
            if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "FOOTACTION") {
                clickAfterElementPresent(LandingPage.search_link(driver));
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                searchBORIS_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                clickAfterElementPresent(ShopPage.size_link(driver));
                searchBORIS_Size();
                clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
                searchBORIS_Zipcode();
                Thread.sleep(oneSec);
                clickAfterElementPresent(ISAPage.findStores_button(driver));
                System.out.println(" STORE LOCATION : ");
                List<WebElement> elements = ISAPage.locationList_grid(driver);
                for (WebElement e : elements) {
                    try {
                        if ((e.getText().contains("PAY NOW, PICK UP TODAY")) || e.getText().contains("PAY NOW, PICK UP IN STORE TODAY")) {
                            System.out.println("=========================");

                            System.out.println(e.getText());

                            Thread.sleep(1000);
                            System.out.println(" THE TEXT 'PAY NOW, PICK UP TODAY' IS VERIFIED. THIS IS A BORIS FULLFILLMENT TYPE ");
                            String elementid = e.getAttribute("id");
                            clickAfterElementPresent(ISAPage.BORISpayNow_button(driver, elementid));
                            break;

                        } else
                            System.out.println("UNABLE TO VERIFY THE TEXT: 'PAY NOW, PICK UP TODAY' FOR BORIS FULLFILLMENT TYPE");

                    } catch (Exception objException) {
                        objException.printStackTrace();
                    }
                    System.out.println("=========================");

                }
                checkout();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT BORIS FULLFILLMENT TYPE");
            }

        } catch (InterruptedException e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR BORIS FULLFILLMENT TYPE");
        }
    }

    public void verifySendToStore() {
        try {
            if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "FOOTACTION") {
                clickAfterElementPresent(LandingPage.search_link(driver));
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                searchS2S_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                clickAfterElementPresent(ShopPage.size_link(driver));
                clickAfterElementPresent(ShopPage.S2S_Size1link(driver));
                clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
                searchS2S_Zipcode();
                Thread.sleep(oneSec);
                clickAfterElementPresent(ISAPage.findStores_button(driver));
                System.out.println(" STORE LOCATION  : ");
                List<WebElement> elements = ISAPage.locationList_grid(driver);
                for (WebElement e : elements) {
                    try {
                        if ((e.getText().toUpperCase().contains("PAY NOW, SHIP TO STORE"))) {
                            System.out.println("=========================");
                            System.out.println(e.getText());
                            Thread.sleep(1000);
                            System.out.println(" THE TEXT 'PAY NOW, SHIP TO STORE' IS VERIFIED. THIS IS A SEND TO STORE FULLFILLMENT TYPE ");
                            //  e.click();
                            String elementid = e.getAttribute("id");
                            clickAfterElementPresent(ISAPage.S2SpayNow_button(driver, elementid));
                            //       System.out.println("the  e.getAttributes  are  "+e.getAttribute("id"));
                            break;
                        } else
                            System.out.println("UNABLE TO VERIFY THE TEXT: 'PAY NOW, SHIP TO STORE' FOR SEND TO STORE  FULLFILLMENT TYPE");

                    } catch (Exception objException) {
                        objException.printStackTrace();
                    }
                    System.out.println("=========================");
                }
                Thread.sleep(oneSec);
                Thread.sleep(oneSec);
                checkout();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT SEND TO STORE  FULLFILLMENT TYPE");
            }
        } catch (InterruptedException e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR SEND TO STORE FULLFILLMENT TYPE");
        }
    }

    public void verifyDropShip() {
        try {
            if (app == "EASTBAY" || app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FOOTLOCKERCA" || app == "FOOTACTION") {
                driver.manage().window().maximize();
                clickAfterElementPresent(LandingPage.search_link(driver));
                Thread.sleep(oneSec);
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                Thread.sleep(oneSec);
                searchDROPSHIP_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                clickAfterElementPresent(ShopPage.DROPSHIP_TextVerify(driver));
                System.out.println(ShopPage.DROPSHIP_TextVerify(driver).getText());
                System.out.println("=========================");
                try {
                    if ((ShopPage.DROPSHIP_TextVerify(driver).getText()).contains(" This item is shipped directly from the manufacturer ")) {
                        System.out.println(" THE TEXT 'This item is shipped directly from the manufacturer' IS VERIFIED FOR THE  DROPSHIP FULLFILLMENT TYPE ");
                    } else
                        System.out.println("UNABLE TO VERIFY THE TEXT: 'This item is shipped directly from the manufacturer' FOR DROPSHIP FULLFILLMENT TYPE");
                    System.out.println("=========================");

                } catch (Exception e) {
                }
                try {
                    clickAfterElementPresent(ShopPage.addToCart_button(driver));
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                checkout();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT DROPSHIP FULLFILLMENT TYPE");
            }
        } catch (
                Exception e)
        {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR DROPSHIP FULLFILLMENT TYPE");
        }
    }

    public void verifySameDayDelivery() {

        try {
            if (app == "FOOTLOCKER") {
                clickAfterElementPresent(LandingPage.search_link(driver));
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                searchSDD_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                clickAfterElementPresent(ShopPage.size_link(driver));
                clickAfterElementPresent(ShopPage.SDD_Size1link(driver));
                clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
                Thread.sleep(oneSec);
                String zipCode = null;
                try {
                    zipCode = readFromExcel(sourceFile, "Product_Zipcode", "F3");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                Thread.sleep(oneSec);
                typeAfterElementPresent(ISAPage.storeLocation_textbox(driver), zipCode);
                System.out.println(" Zipcode  Input  : " + zipCode);

                clickAfterElementPresent(ISAPage.findStores_button(driver));
                System.out.println(" Store Locations displayed  : ");
                List<WebElement> elements = ISAPage.locationList_grid(driver);
                for (WebElement e : elements) {
                    try {
                        System.out.println("=========================");
                        String elementid = e.getAttribute("id");
                        clickAfterElementPresent(ISAPage.SDDLocation_list(driver, elementid));
                        System.out.println("SDDLocation_list:  " + (ISAPage.SDDLocation_list(driver, elementid)).getText());
                        String zipcodeOnMap = ISAPage.SDDLocationOnMap_popUp(driver, elementid).getText();
                        System.out.println("SDDLocationOnMap_popUp:  " + (ISAPage.SDDLocationOnMap_popUp(driver, elementid)).getText());
                        if (zipcodeOnMap.contains(zipCode)) {
                            System.out.println(e.getText());
                            Thread.sleep(1000);
                            System.out.println("THE ZIPCODES MATCH");
                            clickAfterElementPresent(ISAPage.SDDpayNow_button(driver, elementid));
                            System.out.println("SDDpayNow_button:  " + ISAPage.SDDpayNow_button(driver, elementid).getText());
                            System.out.println("=========================");
                            break;
                        } else System.out.println("ZIPCODES DO NOT MATCH");
                        System.out.println("=========================");

                    } catch (Exception objException) {
                        objException.printStackTrace();
                    }
                }
                Thread.sleep(oneSec);
                Thread.sleep(oneSec);


                clickAfterElementPresent(HomePage.viewCartAndCheckout_button(driver));
                clickAfterElementPresent(HomePage.cartCheckout_button(driver));
                setBillingAddress_SDD();
                Thread.sleep(2000);
                System.out.println(" the Ship to home link text is:  " + driver.findElement(By.xpath("//div[@class='shipToHomeInsteadLinkContainer']/a")).getText());
                driver.findElement(By.xpath("//div[@class='shipToHomeInsteadLinkContainer']/a")).click();
                Thread.sleep(1000);
                System.out.println(" the delivery method is: " + PurchasePage.SDD_TextVerify(driver).getText());
                if (PurchasePage.SDD_TextVerify(driver).getText().contains("Delivery TODAY (order by 3PM Local Time) - $5.00") || PurchasePage.SDD_TextVerify(driver).getText().contains("DELIVERY TODAY (ORDER BY 3PM LOCAL TIME) - $5.00")) {
                    clickAfterElementPresent(PurchasePage.SDD_TextVerify(driver));
                    System.out.println(" Delivery option Verified ");
                    System.out.println(" IT IS A SDD PRODUCT ");
                } else {
                    System.out.println(" Delivery option  NOT verified ");
                }
                setPayment();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT SAME DAY DELIVERY FULLFILLMENT TYPE");
            }
        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR SAME DAY DELIVERY FULLFILLMENT TYPE");
        }
    }

    public void verifyDC_Order() {
        try {
            if (app == "EASTBAY" || app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FINAL-SCORE" || app == "FOOTACTION") {
                clickAfterElementPresent(LandingPage.search_link(driver));
                Thread.sleep(oneSec);
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                Thread.sleep(oneSec);
                System.out.println("Search Textbox clicked");
                searchDC_ORDER_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                clickAfterElementPresent(ShopPage.size_link(driver));
                searchBOSS_Size();
                System.out.println("Size selected");
                try {
                    clickAfterElementPresent(ShopPage.addToCart_button(driver));
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                checkout();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                System.out.println(" Order is submitted ");
            } else {
                System.out.println(" This Brand does not support DC ORDER fullfillment type ");
            }
        } catch (
                Exception e) {
            log.info("Unable to verify the DC ORDER fullfillment type");
        }
    }

    public void verifyDC_Order_UAT() {
        try {
            if (app == "EASTBAY" || app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FINAL-SCORE" || app == "FOOTACTION") {
                Thread.sleep(oneSec);
                closeFLCAOverlay_PopUp();
                System.out.println("clicked  1 ");
                closeEmailSignUp_popUp1();
                System.out.println("clicked 2 ");
                closeLastPg_PopUp();
                Thread.sleep(oneSec);
                System.out.println("clicked 3 ");
                closeEmailSignUp_popUp1();
                Thread.sleep(oneSec);
                clickAfterElementPresent(LandingPage.search_link(driver));
                Thread.sleep(oneSec);
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchDC_ORDER_ProductNum();
                Thread.sleep(oneSec);
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                Thread.sleep(oneSec);
                closeFLCAOverlay_PopUp();
                Thread.sleep(oneSec);
                clickAfterElementPresent(ShopPage.size_link(driver));
                Thread.sleep(oneSec);
                searchDC_ORDER_Size();
                Thread.sleep(oneSec);
                System.out.println("Size selected");
                Thread.sleep(oneSec);
                try {
                    clickAfterElementPresent(ShopPage.addToCart_button(driver));
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                checkout_UAT();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                System.out.println(" Order is submitted ");
                setAssertStringValue();
            } else {
                System.out.println(" This Brand does not support DC ORDER fullfillment type ");
            }
        } catch (
                Exception e) {
            log.info("Unable to verify the DC ORDER fullfillment type");
        }
    }

    public void verifyDC_Order_UAT_LFL_FA() {
        try {
            if (app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FINAL-SCORE" || app == "FOOTACTION") {

                Thread.sleep(oneSec);
                closeFLCAOverlay_PopUp();
                System.out.println("clicked  1 ");
                closeEmailSignUp_popUp1();
                System.out.println("clicked 2 ");
                closeLastPg_PopUp();
                Thread.sleep(oneSec);
                System.out.println("clicked 3 ");
                closeEmailSignUp_popUp1();
                Thread.sleep(oneSec);
                clickAfterElementPresent(LandingPage.search_link(driver));
                Thread.sleep(oneSec);
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchDC_ORDER_ProductNum();
                Thread.sleep(oneSec);
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                Thread.sleep(oneSec);
                searchDC_ORDER_Size();
                Thread.sleep(oneSec);
                System.out.println("Size selected");
                Thread.sleep(oneSec);
                try {
                    clickAfterElementPresent(ShopPage.addToCart_button(driver));
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                checkout_UAT();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                System.out.println(" Order is submitted ");
                setAssertStringValue();
            } else {
                System.out.println(" This Brand does not support DC ORDER fullfillment type ");
            }
        } catch (
                Exception e) {
            log.info("Unable to verify the DC ORDER fullfillment type");
        }
    }

    public void verifyBOSS_UAT_EASTBAY() {
        try {
            driver.manage().window().maximize();
            if (app == "EASTBAY" || app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FOOTLOCKERCA" || app == "FOOTACTION") {
                closeFLCAOverlay_PopUp();
                closeEmailSignUp_popUp1();
                closeLastPg_PopUp();
                clickAfterElementPresent(LandingPage.search_link(driver));
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchBOSS_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                System.out.println("Size will be  selected");
                searchBOSS_Size();
                System.out.println("Size clicked");
                Thread.sleep(oneSec);
                System.out.println(ShopPage.BOSS_TextVerify_UAT(driver).getText());
                if (app == "FOOTLOCKERCA") {
                    System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                } else {
                    try {
                        Thread.sleep(oneSec);
                        String BossTextString = ShopPage.BOSS_TextVerify_UAT(driver).getText().toUpperCase();
                        if (BossTextString.contains("SHIPS TO THE 48 CONTIGUOUS UNITED STATES") || BossTextString.contains("* ONLY SHIPS TO LOWER 48 STATES") || BossTextString.contains("ONLY SHIPS TO LOWER 48 STATES. NO GUARANTEED FOR CHRISTMAS.")) {
                            System.out.println(ShopPage.BOSS_TextVerify_UAT(driver).getText());
                            System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                        } else System.out.println(" UNABLE TO VERIFY THE TEXT FOR THE PRODUCT. ");
                    } catch (Exception e1) {
                        log.info("UNABLE TO VERIFY THE TEXT: '* Only ships to lower 48 states' FOR BOSS FULLFILLMENT TYPE");
                    }
                }
                try {
                    clickAfterElementPresent(ShopPage.addToCart_button(driver));
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                checkout_UAT();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder_UAT();
                System.out.println(" Order is submitted ");
                setAssertStringValue();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT BOSS FULLFILLMENT TYPE");
            }
        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR BOSS FULLFILLMENT TYPE");
        }
    }

    public void verifyBOSS_UAT_LFL_FA() {
        try {
            driver.manage().window().maximize();
            if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FOOTACTION") {
                Thread.sleep(oneSec);
                closeFLCAOverlay_PopUp();
                System.out.println("clicked  1 ");
                closeEmailSignUp_popUp1();
                System.out.println("clicked 2 ");
                closeLastPg_PopUp();
                Thread.sleep(oneSec);
                System.out.println("clicked 3 ");
                closeEmailSignUp_popUp1();
                clickAfterElementPresent(LandingPage.search_link(driver));
                Thread.sleep(oneSec);
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchBOSS_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                closeFLCAOverlay_PopUp();
                Thread.sleep(oneSec);
                System.out.println("Size will be  selected");
                searchBOSS_Size();
                System.out.println("Size clicked");
                Thread.sleep(oneSec);
                System.out.println(ShopPage.BOSS_TextVerify_UAT(driver).getText());
                if (app == "FOOTLOCKERCA") {
                    System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                } else {
                    try {
                        Thread.sleep(oneSec);
                        String BossTextString = ShopPage.BOSS_TextVerify_UAT(driver).getText().toUpperCase();
                        if (BossTextString.contains("SHIPS TO THE 48 CONTIGUOUS UNITED STATES") || BossTextString.contains("* ONLY SHIPS TO LOWER 48 STATES") || BossTextString.contains("ONLY SHIPS TO LOWER 48 STATES. NO GUARANTEED FOR CHRISTMAS.")) {
                            System.out.println(ShopPage.BOSS_TextVerify_UAT(driver).getText());
                            System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                        } else System.out.println(" UNABLE TO VERIFY THE TEXT FOR THE PRODUCT. ");
                    } catch (Exception e1) {
                        log.info("UNABLE TO VERIFY THE TEXT: '* Only ships to lower 48 states' FOR BOSS FULLFILLMENT TYPE");
                    }
                }
                Thread.sleep(oneSec);
                clickAfterElementPresent(ShopPage.addToCart_button(driver));
                Thread.sleep(oneSec);
                checkout_UAT();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                setAssertStringValue();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT BOSS FULLFILLMENT TYPE");
            }
        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR BOSS FULLFILLMENT TYPE");
        }
    }

    public void verifyBOSS_UAT() {
        try {
            driver.manage().window().maximize();
            if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "SIX02" || app == "FOOTACTION") {
                Thread.sleep(oneSec);
                closeFLCAOverlay_PopUp();
                System.out.println("clicked  1 ");
                closeEmailSignUp_popUp1();
                System.out.println("clicked 2 ");
                closeLastPg_PopUp();
                Thread.sleep(oneSec);
                System.out.println("clicked 3 ");
                closeEmailSignUp_popUp1();
                clickAfterElementPresent(LandingPage.search_link(driver));
                Thread.sleep(oneSec);
                System.out.println("Search link clicked ");
                clickAfterElementPresent(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchBOSS_ProductNum();
                clickAfterElementPresent(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                closeFLCAOverlay_PopUp();
                Thread.sleep(oneSec);
                if (!(app == "LADYFOOTLOCKER") || !(app == "FOOTACTION"))
                    clickAfterElementPresent(ShopPage.size_link(driver));
                Thread.sleep(oneSec);
                Thread.sleep(oneSec);
                System.out.println("Size will be  selected");
                searchBOSS_Size();
                System.out.println("Size clicked");
                Thread.sleep(oneSec);
                System.out.println(ShopPage.BOSS_TextVerify_UAT(driver).getText());
                if (app == "FOOTLOCKERCA") {
                    System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                } else {
                    try {
                        Thread.sleep(oneSec);
                        String BossTextString = ShopPage.BOSS_TextVerify_UAT(driver).getText().toUpperCase();
                        if (BossTextString.contains("SHIPS TO THE 48 CONTIGUOUS UNITED STATES") || BossTextString.contains("* ONLY SHIPS TO LOWER 48 STATES") || BossTextString.contains("ONLY SHIPS TO LOWER 48 STATES. NO GUARANTEED FOR CHRISTMAS.")) {
                            System.out.println(ShopPage.BOSS_TextVerify_UAT(driver).getText());
                            System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                        } else System.out.println(" UNABLE TO VERIFY THE TEXT FOR THE PRODUCT. ");
                    } catch (Exception e1) {
                        log.info("UNABLE TO VERIFY THE TEXT: '* Only ships to lower 48 states' FOR BOSS FULLFILLMENT TYPE");
                    }
                }
                Thread.sleep(oneSec);
                clickAfterElementPresent(ShopPage.addToCart_button(driver));
                Thread.sleep(oneSec);
                checkout_UAT();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                setAssertStringValue();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT BOSS FULLFILLMENT TYPE");
            }

        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR BOSS FULLFILLMENT TYPE");
        }
    }

    public void setAssertStringValue() {
        Date date = new Date();
        System.out.println(app + "_BOSS_" + orderNumber() + "_" + date);
    }

    public void timeStamp() {
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, new Locale("en", "EN"));
        String formattedDate = df.format(new Date());
    }

    public void currentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println(sdf.format(cal.getTime()));
    }

    public void verifyBOSS_UAT_FOOTLOCKER_CA() {
        try {
            driver.manage().window().maximize();
            if (app == "FOOTLOCKERCA") {
                closeLastPg_PopUp();
                clickAfterElementPresent_UAT(LandingPage.search_link(driver));
                System.out.println("Search link clicked ");
                clickAfterElementPresent_UAT(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchBOSS_ProductNum();
                clickAfterElementPresent_UAT(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                closeEmailSignUp_popUp2();
                moveToElement(ShopPage.size_link(driver));
                Thread.sleep(oneSec);
                searchBOSS_Size();
                Thread.sleep(oneSec);
                System.out.println("Size selected");
                clickAfterElementPresent_UAT(ShopPage.addToCart_button(driver));
                checkout_UAT();
                System.out.println(" Checkout completed ");
                setBillingAddress();
                System.out.println(" Billing Address completed ");
                setDeliveryOptions();
                System.out.println(" Delivery option selected ");
                setPayment();
                System.out.println(" Payment details completed ");
                submitOrder();
                System.out.println(" Order is submitted ");
                setAssertStringValue();
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT BOSS FULLFILLMENT TYPE");
            }

        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR BOSS FULLFILLMENT TYPE");
        }
    }

    public void verifyHTMLTagOnSizeWidget_UAT() {
        try {
            if (app == "LADYFOOTLOCKER" || app == "FOOTACTION") {
                List<WebElement> elements = ShopPage.size1link_LFL_FA(driver);
                for (WebElement e : elements) {
                    System.out.println(e.getText());
                    String str = e.getText();
                    e.click();
                    if (ShopPage.BOSS_TextVerify(driver).getText().contains(str + " * Only ships to lower 48 states")) {
                        System.out.println(ShopPage.BOSS_TextVerify(driver).getText());
                        System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                        break;
                    }
                }
            }
        } catch (Exception e1) {
            log.info("UNABLE TO VERIFY THE TEXT: '* Only ships to lower 48 states' FOR BOSS FULLFILLMENT TYPE");
        }
    }

    public void verifyTextForBOSS_UAT() {
        try {
            if (app == "FOOTLOCKERCA") moveToElement(ShopPage.size_link(driver));
            else clickAfterElementPresent_UAT(ShopPage.size_link(driver));
            Thread.sleep(oneSec);
            clickAfterElementPresent_UAT(ShopPage.BOSS_Size1link(driver));
            Thread.sleep(oneSec);
            System.out.println("Size selected");
            if (app == "FOOTLOCKERCA") {
                System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
            } else {
                try {
                    clickAfterElementPresent_UAT(ShopPage.BOSS_TextVerify(driver));
                    System.out.println(ShopPage.BOSS_TextVerify(driver).getText());
                    if ((ShopPage.BOSS_TextVerify(driver).getText()).contains("*Ships to the 48 contiguous United States") || (ShopPage.BOSS_TextVerify(driver).getText()).contains("* Only ships to lower 48 states"))
                        System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A BOSS PRODUCT ");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE TEXT: '* Only ships to lower 48 states' FOR BOSS FULLFILLMENT TYPE");
        }

    }

    public void moveToElement(WebElement element) {
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
        log.info("Moved to Element ");
    }

    public void verifySingleStoreInventory_UAT() {
        try {
            driver.manage().window().maximize();
            if (app == "FOOTLOCKER" || app == "LADYFOOTLOCKER" || app == "KIDSFOOTLOCKER" || app == "CHAMPSSPORTS" || app == "FOOTACTION") {
                clickAfterElementPresent_UAT(LandingPage.search_link(driver));
                System.out.println("Search link clicked ");
                clickAfterElementPresent_UAT(LandingPage.search_Textbox(driver));
                System.out.println("Search Textbox clicked");
                searchSSI_ProductNum();
                clickAfterElementPresent_UAT(LandingPage.submit_link(driver));
                System.out.println("Search Submit button clicked");
                if (app == "LADYFOOTLOCKER" || app == "FOOTACTION") {
                    List<WebElement> elements = ShopPage.size1link_LFL_FA(driver);
                    for (WebElement e : elements) {
                        System.out.println(e.getText());
                        e.click();
                        if (ShopPage.SSI_TextVerify(driver).getText().contains("AVAILABLE FOR PURCHASE IN SELECT STORES") || ShopPage.SSI_TextVerify(driver).getText().contains("Available for purchase in select stores")) {
                            // if (ShopPage.SSI_TextVerify(driver).getText().contains("Available for purchase in select stores")) {
                            System.out.println(ShopPage.SSI_TextVerify(driver).getText());
                            break;
                        } else
                            System.out.println("UNABLE TO VERIFY THE TEXT: 'AVAILABLE FOR PURCHASE IN SELECT STORES' FOR SINGLE STORE INVENTORY FULLFILLMENT TYPE");
                    }
                } else {
                    Thread.sleep(1000);
                    clickAfterElementPresent_UAT(ShopPage.size_link(driver));
                    Thread.sleep(1000);
                    clickAfterElementPresent_UAT(ShopPage.SSI_Size1link(driver));
                    System.out.println(ShopPage.SSI_Size1link(driver).getText());
                    Thread.sleep(1000);
                    System.out.println(" THE TEXT IS VERIFIED FOR THE PRODUCT. THIS IS A SSI PRODUCT ");
                }
                Thread.sleep(1000);
                clickAfterElementPresent_UAT(ShopPage.StorePickup_radioButton(driver));
                searchSSI_Zipcode();
                clickAfterElementPresent_UAT(ISAPage.findStores_button(driver));
                List<WebElement> elements = ISAPage.locationList_grid(driver);
                for (WebElement e : elements) {
                    int j = 1;
                    System.out.println("=========================");
                    System.out.println(" STORE LOCATION : " + j);
                    System.out.println(e.getText());
                    Thread.sleep(oneSec);
                    j++;
                }
                System.out.println("=========================");
            } else {
                System.out.println(" THIS BRAND DOES NOT SUPPORT SSI FULLFILLMENT TYPE");
            }

        } catch (Exception e) {
            log.info("UNABLE TO VERIFY THE BRAND AND THE PRODUCT FOR SINGLE STORE INVENTORY FULLFILLMENT TYPE");
        }
    }
}