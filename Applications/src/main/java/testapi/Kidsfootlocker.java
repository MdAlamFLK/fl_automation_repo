package testapi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.ISAPage;
import pageobjects.LandingPage;
import pageobjects.ShopPage;

import java.util.List;

import static pageobjects.LandingPage.search_Textbox;

/**
 * Created by malam2 on 2/1/17.
 */
public class Kidsfootlocker extends SubBase {

    public void runGeneral() throws InterruptedException {
        runDCOrder();
    }

    public void runDCOrder() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchDC_ORDER_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "PROD") scrollDown();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchDC_ORDER_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_DCOrder(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(assertStringValue, "In Stock");
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runBOSS() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBOSS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closePopUp_1();
        if (env == "PROD") scrollDown();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchBOSS_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_BOSS(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("48"));
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runS2S() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchS2S_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "PROD") scrollDown();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchS2S_Size() +"']")));
        clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
        typeAfterElementPresent(ISAPage.storeLocation_textbox(driver), searchS2S_Zipcode());
        clickAfterElementPresent(ISAPage.findStores_button(driver));
        clickAfterElementPresent(ISAPage.S2SpayNow_button(driver, searchStoreButtonContaining("PAY NOW, SHIP TO STORE")));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("PAY NOW, SHIP TO STORE"));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runBORIS() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBORIS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeFLCAOverlay_PopUp();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchBORIS_Size() +"']")));
        clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
        typeAfterElementPresent(ISAPage.storeLocation_textbox(driver), searchBORIS_Zipcode());
        Thread.sleep(shortWait);
        clickAfterElementPresent(ISAPage.findStores_button(driver));
        Thread.sleep(shortWait);
        clickAfterElementPresent(ISAPage.BORISpayNow_button(driver, searchStoreButtonContaining("PAY NOW, PICK UP TODAY")));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("PAY NOW, PICK UP TODAY"));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runSSI() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchSSI_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeFLCAOverlay_PopUp();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchSSI_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_S2S(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("Available for purchase in select stores"));
    }

    public void runDropship() throws InterruptedException {
        verifyDropShip();
    }
}