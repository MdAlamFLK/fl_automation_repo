package testapi;

import org.openqa.selenium.By;
import org.testng.Assert;
import pageobjects.LandingPage;
import pageobjects.ShopPage;

import static pageobjects.LandingPage.search_Textbox;

/**
 * Created by malam2 on 2/1/17.
 */
public class Six02 extends SubBase {

    public void runGeneral() throws InterruptedException {
        runDCOrder();
    }

    public void runDCOrder() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchDC_ORDER_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchDC_ORDER_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_DCOrder(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(assertStringValue, "In Stock");
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runBOSS() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBOSS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closePopUp_1();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchBOSS_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_BOSS(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("48"));
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runDropship() throws InterruptedException {
        verifyDropShip();
    }
}
