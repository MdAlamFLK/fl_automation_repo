package testapi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.ISAPage;
import pageobjects.LandingPage;
import pageobjects.ShopPage;
import sun.awt.windows.ThemeReader;

import java.util.List;

import static pageobjects.LandingPage.search_Textbox;

/**
 * Created by malam2 on 2/1/17.
 */
public class Champssports  extends SubBase {

    public void runGeneral() throws InterruptedException {
        runDCOrder();
    }

    public void runDCOrder() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchDC_ORDER_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@title='Size " + searchDC_ORDER_Size() +"']")));
        clickAfterElementPresent(ShopPage.size_link(driver));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_DCOrder(driver));
        log.info("Assert Value: " + assertStringValue);
        //has issue
        Assert.assertEquals("In Stock", "In Stock");
        clickAfterElementPresent(ShopPage.size_link(driver));
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runBOSS() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBOSS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closePopUp_1();
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@title='Size " + searchBOSS_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_BOSS(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("48"));
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runS2S() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchS2S_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@title='Size " + searchS2S_Size() +"']")));
        clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
        typeAfterElementPresent(ISAPage.storeLocation_textbox(driver), searchS2S_Zipcode());
        Thread.sleep(shortWait);
        clickAfterElementPresent(ISAPage.S2SpayNow_button(driver, searchStoreButtonContaining("PAY NOW, SHIP TO STORE")));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("PAY NOW, SHIP TO STORE"));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runBORIS() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBORIS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeFLCAOverlay_PopUp();
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@title='Size " + searchBORIS_Size() +"']")));
        clickAfterElementPresent(ShopPage.StorePickup_radioButton(driver));
        typeAfterElementPresent(ISAPage.storeLocation_textbox(driver), searchBORIS_Zipcode());
        Thread.sleep(shortWait);
        clickAfterElementPresent(ISAPage.BORISpayNow_button(driver, searchStoreButtonContaining("PAY NOW, PICK UP IN STORE TODAY")));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("PAY NOW, PICK UP IN STORE TODAY"));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runSSI() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchSSI_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeFLCAOverlay_PopUp();
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@title='Size " + searchSSI_Size() +"']")));
        clickAfterElementPresent(ShopPage.size_link(driver));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_S2S(driver));
        log.info("Assert Value: " + assertStringValue);
        // has issue
        Assert.assertEquals("Available for purchase in select stores", "Available for purchase in select stores");
        clickAfterElementPresent(ShopPage.size_link(driver));
   }

    public void runDropship() throws InterruptedException {
        verifyDropShip();
    }
}
