package testapi;

import org.openqa.selenium.By;
import org.testng.Assert;
import pageobjects.LandingPage;
import pageobjects.ShopPage;

import static pageobjects.LandingPage.search_Textbox;

/**
 * Created by malam2 on 2/1/17.
 */
public class FootlockerCa extends SubBase {

    public void runGeneral() throws InterruptedException {
        addToCart();
    }

    public void runBOSS() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp2();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(LandingPage.search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBOSS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeEmailSignUp_popUp2();
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchBOSS_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_BOSS(driver));
        Assert.assertEquals(true, assertStringValue.contains("Ships Free!"));
        log.info("Assert Value: " + assertStringValue);
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runDropship() throws InterruptedException {

    }
}
