package testapi;

import org.openqa.selenium.By;
import org.testng.Assert;
import pageobjects.LandingPage;
import pageobjects.ShopPage;

import static pageobjects.LandingPage.search_Textbox;

/**
 * Created by malam2 on 2/1/17.
 */
public class FinalScore extends SubBase {

    public void runGeneral() throws InterruptedException {
        runDCOrder();
    }

    public void runDCOrder() throws InterruptedException {
        typeAfterElementPresent(search_Textbox(driver), searchDC_ORDER_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@title='"+ searchDC_ORDER_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_DCOrder(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(assertStringValue, "In Stock");
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }
}
