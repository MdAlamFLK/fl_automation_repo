package testapi;

import org.openqa.selenium.By;
import org.testng.Assert;
import pageobjects.LandingPage;
import pageobjects.ShopPage;

import static pageobjects.LandingPage.search_Textbox;

public class Eastbay extends SubBase {

    public void runGeneral() throws InterruptedException {
        login();
    }

    public void runDCOrder() throws InterruptedException {
        if (env == "UAT") closeEmailSignUp_popUp1();
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchDC_ORDER_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closeEmailSignUp_popUp2();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchDC_ORDER_Size() +"']")));
        clickAfterElementPresent(ShopPage.size_link(driver));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_DCOrder(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(assertStringValue, "In Stock");
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        if (env == "UAT") closeEmailSignUp_popUp2();
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runBOSS() throws InterruptedException {
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchBOSS_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closePopUp_1();
        clickAfterElementPresent(ShopPage.size_link(driver));
        clickAfterElementPresent(driver.findElement(By.xpath("//*[@value='"+ searchBOSS_Size() +"']")));
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_BOSS(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("48"));
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }

    public void runDropship() throws InterruptedException {
        clickAfterElementPresent(LandingPage.search_link(driver));
        clickAfterElementPresent(search_Textbox(driver));
        typeAfterElementPresent(search_Textbox(driver), searchDROPSHIP_ProductNum());
        clickAfterElementPresent(LandingPage.submit_link(driver));
        if (env == "UAT") closePopUp_1();
        assertStringValue = getTextFromLabelAfterElementPresent(ShopPage.Assert_DROPSHIP(driver));
        log.info("Assert Value: " + assertStringValue);
        Assert.assertEquals(true, assertStringValue.contains("This item is shipped directly from the manufacturer"));
        clickAfterElementPresent(ShopPage.addToCart_button(driver));
        if (env == "UAT") closeEmailSignUp_popUp2();
        checkout();
        setBillingAddress();
        setDeliveryOptions();
        setPayment();
        submitOrder();
    }
}